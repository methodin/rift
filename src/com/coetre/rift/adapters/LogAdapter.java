package com.coetre.rift.adapters;

import java.util.List;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.coetre.rift.R;
import com.coetre.rift.entities.TaskLog;

public class LogAdapter extends ArrayAdapter<TaskLog> {
	private List<TaskLog> logs;

	public LogAdapter(Context context, List<TaskLog> logs) {
		super(context, android.R.layout.simple_list_item_multiple_choice, logs);
		this.logs = logs;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (this.logs.size() > 0) {
			final TaskLog log = this.logs.get(position);
			if (log != null) {
				final TextView textView;
				final TextView subView;
                final TextView invoiceView;

				// Create new view
				if (convertView == null) {
					convertView = LayoutInflater.from(getContext()).inflate(
							R.layout.row_log, parent, false);

					textView = (TextView) convertView.findViewById(android.R.id.text1);
                    subView = (TextView) convertView.findViewById(android.R.id.text2);
                    invoiceView = (TextView) convertView.findViewById(R.id.invoice);

					convertView.setTag(new ViewHolder(textView,subView,invoiceView,log));
				} else {
					ViewHolder viewHolder = (ViewHolder) convertView.getTag();
					textView = viewHolder.getTextView();
					subView = viewHolder.getSubView();
                    invoiceView = viewHolder.getInvoiceView();
				}

				textView.setText("Time logged: "+log.getFormattedTime());
				subView.setText(DateFormat.format("M/dd/yy h:mm:ssa", log.getEndDate()));

                if(log.getInvoiceRecord() != null) {
                    invoiceView.setVisibility(View.VISIBLE);
                    invoiceView.setText("Last invoiced "+log.getInvoiceRecord().getInvoiceClient().getInvoice().getFormattedDate());
                } else {
                    invoiceView.setVisibility(View.GONE);
                }

				return convertView;
			}
		}
		return null;
	}

	@Override
	public int getCount() {
		return this.logs.size();
	}

	@Override
	public TaskLog getItem(int position) {
		return this.logs.get(position);
	}

    @Override
    public boolean isEnabled(int position) {
        return this.getItem(position).getInvoiceRecord() == null;
    }

    public void setItems(List<TaskLog> logs) {
		this.logs = logs;
		notifyDataSetChanged();
	}

    /**
	 * ViewHolder to keep track of object references
	 * 
	 * @author Derek
	 * 
	 */
	private static class ViewHolder {
		private final TextView textView;
		private final TextView subView;
        private final TextView invoiceView;
		private final TaskLog log;

		public ViewHolder(TextView textView, TextView subView, TextView invoiceView, TaskLog log) {
			this.textView = textView;
			this.subView = subView;
            this.invoiceView = invoiceView;
			this.log = log;
		}

		public TextView getTextView() {
			return this.textView;
		}

        public TextView getInvoiceView() {
            return this.invoiceView;
        }
		
		public TextView getSubView() {
			return this.subView;
		}
	}
}