package com.coetre.rift.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.coetre.rift.MainActivity;
import com.coetre.rift.R;
import com.coetre.rift.entities.Task;

import java.util.List;

public class TaskAdapter extends ArrayAdapter<Task>  {
	private int resource;
    private Task currentTask;

	public TaskAdapter(Context context, int resource, List<Task> tasks, Task currentTask) {
		super(context, resource, tasks);
		this.resource = resource;
        this.currentTask = currentTask;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (this.getCount() > 0) {
			final Task task = this.getItem(position);
			if (task != null) {
				final TextView textView;

				// Create new view
				if (convertView == null) {
					convertView = LayoutInflater.from(getContext()).inflate(
							resource, parent, false);

					textView = (TextView) convertView
							.findViewById(R.id.taskDescription);

					convertView.setTag(new ViewHolder(textView, task));

				} else {
					ViewHolder viewHolder = (ViewHolder) convertView.getTag();
					textView = viewHolder.getTextView();
					viewHolder.setTask(task);
				}

                if(task.getParent() != null) {
                    textView.setText(" >> "+task.getName());
                } else {
                    textView.setText(task.getName());
                }

				return convertView;
			}
		}
		return null;
	}

    /**
     * Class to hold view references
     */
    private class ViewHolder {
        final private TextView textView;
        private Task task;

        public ViewHolder(TextView textView, Task task) {
            this.textView = textView;
            this.task = task;
        }

        private TextView getTextView() {
            return textView;
        }

        private void setTask(Task task) {
            this.task = task;
        }
    }
}