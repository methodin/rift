package com.coetre.rift.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.coetre.rift.entities.Task;
import com.coetre.rift.views.TaskCardView;

import java.util.List;

public class TaskCardAdapter extends ArrayAdapter<Task> {
    private List<Task> tasks;
    private int resource;

    public TaskCardAdapter(Context context, int resource, List<Task> tasks) {
        super(context, resource, tasks);
        this.resource = resource;
        setTasks(tasks);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Create new view
        if (convertView == null) {
            convertView = new TaskCardView(getContext(), tasks.get(position), resource);
        } else {
            ((TaskCardView) convertView).setTask(tasks.get(position));
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return tasks != null ? tasks.size() : 0;
    }

    @Override
    public Task getItem(int position) {
        return tasks != null ? tasks.get(position) : null;
    }

    /**
     * Set the list of tasks
     *
     * @param tasks
     */
    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
        notifyDataSetChanged();
    }

    /**
     * Fetches the list of tasks
     *
     * @return
     */
    public List<Task> getTasks() {
        return this.tasks;
    }
}