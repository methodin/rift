package com.coetre.rift.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.coetre.rift.R;
import com.coetre.rift.entities.Invoice;
import com.coetre.rift.entities.InvoiceClient;
import com.coetre.rift.utils.Config;

import java.util.List;

public class InvoiceAdapter extends ArrayAdapter<Invoice> {
    public InvoiceAdapter(Context context, int resource, List<Invoice> invoices) {
        super(context, resource, invoices);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Invoice invoice = this.getItem(position);
        if (invoice != null) {
            final TextView amountView;
            final TextView clientView;
            final TextView timeView;
            final TextView dateView;

            // Create new view
            if (convertView == null) {
                int resource = R.layout.row_invoice;
                convertView = LayoutInflater.from(getContext()).inflate(resource, parent, false);

                amountView = (TextView) convertView.findViewById(R.id.amount);
                timeView = (TextView) convertView.findViewById(R.id.time);
                clientView = (TextView) convertView.findViewById(R.id.clients);
                dateView = (TextView) convertView.findViewById(R.id.date);

                amountView.setTypeface(Config.thin);
                timeView.setTypeface(Config.thin);
                clientView.setTypeface(Config.thin);

                convertView.setTag(new ViewHolder(amountView, clientView, timeView, dateView));
            } else {
                ViewHolder viewHolder = (ViewHolder) convertView.getTag();
                amountView = viewHolder.getAmountView();
                timeView = viewHolder.getTimeView();
                clientView = viewHolder.getClientView();
                dateView = viewHolder.getDateView();
            }

            amountView.setText(invoice.getFormattedTotal());
            timeView.setText(invoice.getFormattedTime());
            dateView.setText(invoice.getFormattedDate());

            String clients = "";
            for(InvoiceClient invoiceClient : invoice.getInvoiceClients()) {
                clients += (clients.equals("")?"":", ")+invoiceClient.getClient().getName();
            }
            clientView.setText(clients);

            return convertView;
        }

        return null;
    }

    /**
     * ViewHolder to keep track of object references
     *
     * @author Derek
     */
    private static class ViewHolder {
        private final TextView amountView;
        private final TextView clientView;
        private final TextView timeView;
        private final TextView dateView;

        public ViewHolder(TextView amountView, TextView clientView, TextView timeView, TextView dateView) {
            this.amountView = amountView;
            this.clientView = clientView;
            this.timeView = timeView;
            this.dateView = dateView;
        }

        private TextView getAmountView() {
            return amountView;
        }

        private TextView getClientView() {
            return clientView;
        }

        private TextView getTimeView() {
            return timeView;
        }

        private TextView getDateView() {
            return dateView;
        }
    }
}