package com.coetre.rift.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.activeandroid.util.SQLiteUtils;
import com.coetre.rift.R;
import com.coetre.rift.entities.Client;

import java.util.ArrayList;
import java.util.List;

public class ColorAdapter extends ArrayAdapter<String> {
    private List<String> colors;
    private ArrayList<String> usedColors = new ArrayList<String>();

    public ColorAdapter(Context context, List<String> colors) {
        super(context, android.R.layout.simple_list_item_1, colors);

        List<Client> clients = SQLiteUtils.rawQuery(Client.class, "SELECT DISTINCT Color FROM Client", new String[0]);
        for (Client client : clients) {
            usedColors.add(client.getColor());
        }

        this.colors = colors;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (this.colors.size() > 0) {
            final String color = this.colors.get(position);
            if (color != null) {
                final TextView usedView;
                final LinearLayout container;

                // Create new view
                if (convertView == null) {
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_color, parent, false);

                    usedView = (TextView) convertView.findViewById(android.R.id.text1);
                    container = (LinearLayout) convertView.findViewById(R.id.container);

                    convertView.setTag(new ViewHolder(usedView, container));
                } else {
                    ViewHolder viewHolder = (ViewHolder) convertView.getTag();
                    usedView = viewHolder.getTextView();
                    container = viewHolder.getContainer();
                }

                usedView.setText(usedColors.contains(color) ? "Used" : "");
                container.setBackgroundColor(Color.parseColor(color));

                return convertView;
            }
        }
        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (this.colors.size() > 0) {
            final String color = this.colors.get(position);
            if (color != null) {
                final TextView usedView;
                final LinearLayout container;

                // Create new view
                if (convertView == null) {
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_color, parent, false);

                    usedView = (TextView) convertView.findViewById(android.R.id.text1);
                    container = (LinearLayout) convertView.findViewById(R.id.container);

                    convertView.setTag(new ViewHolder(usedView, container));
                } else {
                    ViewHolder viewHolder = (ViewHolder) convertView.getTag();
                    usedView = viewHolder.getTextView();
                    container = viewHolder.getContainer();
                }

                usedView.setText("");
                container.setBackgroundColor(Color.parseColor(color));

                return convertView;
            }
        }
        return null;
    }

    @Override
    public int getCount() {
        return this.colors.size();
    }

    @Override
    public String getItem(int position) {
        return this.colors.get(position);
    }

    /**
     * ViewHolder to keep track of object references
     *
     * @author Derek
     */
    private static class ViewHolder {
        private final TextView textView;
        private final LinearLayout container;

        public ViewHolder(TextView textView, LinearLayout container) {
            this.textView = textView;
            this.container = container;
        }

        public TextView getTextView() {
            return this.textView;
        }

        public LinearLayout getContainer() {
            return this.container;
        }
    }
}