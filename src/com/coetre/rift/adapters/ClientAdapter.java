package com.coetre.rift.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.coetre.rift.R;
import com.coetre.rift.entities.Client;
import com.coetre.rift.entities.Task;
import com.coetre.rift.utils.Config;

public class ClientAdapter extends ArrayAdapter<Client> {
	public final static int MODE_SELECT = 0;
	public final static int MODE_LIST = 1;

	private List<Client> clients;
	private int mode = MODE_SELECT;
    private Task taskReference = new Task();

	public ClientAdapter(Context context, List<Client> clients, boolean includeEmptyOption, int mode) {
		super(context, android.R.layout.simple_list_item_1, clients);
		this.mode = mode;
		this.clients = clients;
		if (includeEmptyOption) {
			this.clients.add(0, new Client("No client", "", 0));
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (this.clients.size() > 0) {
			final Client client = this.clients.get(position);
			if (client != null) {
				final TextView textView;
				final TextView alternateView;
                final TextView clientLabel;
                final TextView rateView;
                final TextView unpaidView;
                final TextView timeView;
                final TextView paidLabel;

				// Create new view
				if (convertView == null) {
					int resource = R.layout.row_client_select;
					if(mode == MODE_LIST) {
						resource = R.layout.row_client_list;
					}
					convertView = LayoutInflater.from(getContext()).inflate(resource, parent, false);

					textView = (TextView) convertView.findViewById(android.R.id.text1);
                    timeView = (TextView) convertView.findViewById(R.id.time);
					alternateView = (TextView) convertView.findViewById(android.R.id.text2);
                    rateView = (TextView) convertView.findViewById(R.id.rate);
                    unpaidView = (TextView) convertView.findViewById(R.id.unpaid);
                    clientLabel = (TextView) convertView.findViewById(R.id.clientLabel);
                    paidLabel = (TextView) convertView.findViewById(R.id.paidLabel);

                    if(mode == MODE_LIST) {
                        timeView.setTypeface(Config.thin);
                        paidLabel.setTypeface(Config.thin);
                        ((TextView)convertView.findViewById(R.id.timeLabel)).setTypeface(Config.thin);
                        rateView.setTypeface(Config.thin);
                        unpaidView.setTypeface(Config.thin);
                    }

					convertView.setTag(new ViewHolder(textView, alternateView, clientLabel, client, rateView, unpaidView, timeView, paidLabel));
				} else {
					ViewHolder viewHolder = (ViewHolder) convertView.getTag();
					textView = viewHolder.getTextView();
                    rateView = viewHolder.getRateView();
                    unpaidView = viewHolder.getUnpaidView();
					alternateView = viewHolder.getAlternateView();
                    clientLabel = viewHolder.getClientLabel();
                    timeView = viewHolder.getTimeView();
                    paidLabel = viewHolder.getPaidLabel();
				}

                textView.setText(client.getName());
                int seconds = 0;
				if(alternateView != null) {
                    List<Task> tasks = client.getTasks();

                    for(Task task : tasks) {
                        seconds += task.getSeconds();
                    }

                    if(seconds > 0) {
                        unpaidView.setText(client.getFormattedRate(seconds));
                        unpaidView.setVisibility(View.VISIBLE);
                    } else {
                        unpaidView.setVisibility(View.GONE);
                    }
					alternateView.setText(client.getTaskCount()+" task"+(tasks.size()==1?"":"s"));
				}

                if(client.getColor() != null && !client.getColor().equals("")) {
                    clientLabel.setVisibility(View.VISIBLE);
                    clientLabel.setBackgroundColor(client.getColorAsInt());
                } else {
                    clientLabel.setVisibility(View.GONE);
                }

				if(mode == MODE_LIST) {
                    timeView.setText(taskReference.getFormattedTime(seconds));
                    if(client.getRate() == 0) {
                        rateView.setVisibility(View.GONE);
                        paidLabel.setVisibility(View.GONE);
                    } else {
                        rateView.setText("$"+Integer.toString(client.getRate())+"/hr");
                        rateView.setVisibility(View.VISIBLE);
                        paidLabel.setVisibility(View.VISIBLE);
                    }
				}

				return convertView;
			}
		}
		return null;
	}

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (this.clients.size() > 0) {
            final Client client = this.clients.get(position);
            if (client != null) {
                final TextView textView;
                final TextView clientLabel;

                // Create new view
                if (convertView == null) {
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_client_select, parent, false);

                    textView = (TextView) convertView.findViewById(android.R.id.text1);
                    clientLabel = (TextView) convertView.findViewById(R.id.clientLabel);

                    convertView.setTag(new ViewHolder(textView, null, clientLabel, client, null, null, null, null));
                } else {
                    ViewHolder viewHolder = (ViewHolder) convertView.getTag();
                    textView = viewHolder.getTextView();
                    clientLabel = viewHolder.getClientLabel();
                }

                textView.setText(client.getName());
                if(client.getColor() != null && !client.getColor().equals("")) {
                    clientLabel.setVisibility(View.VISIBLE);
                    clientLabel.setBackgroundColor(client.getColorAsInt());
                } else {
                    clientLabel.setVisibility(View.GONE);
                }

                return convertView;
            }
        }
        return null;
    }

	@Override
	public int getCount() {
		return this.clients.size();
	}

	@Override
	public Client getItem(int position) {
		return this.clients.get(position);
	}

    /**
	 * ViewHolder to keep track of object references
	 * 
	 * @author Derek
	 * 
	 */
	private static class ViewHolder {
		private final TextView textView;
		private final TextView alternateView;
        private final Client client;
        private final TextView clientLabel;
        private final TextView rateView;
        private final TextView unpaidView;
        private final TextView timeView;
        private final TextView paidLabel;

		public ViewHolder(TextView textView, TextView alternateView, TextView clientLabel, Client client, TextView rateView, TextView unpaidView, TextView timeView, TextView paidLabel) {
			this.textView = textView;
			this.alternateView = alternateView;
            this.client = client;
            this.clientLabel = clientLabel;
            this.rateView = rateView;
            this.unpaidView = unpaidView;
            this.timeView = timeView;
            this.paidLabel = paidLabel;
		}

		public TextView getTextView() {
			return this.textView;
		}
		
		public TextView getAlternateView() {
            return this.alternateView;
        }

        public TextView getRateView() {
            return this.rateView;
        }

        public TextView getUnpaidView() {
            return this.unpaidView;
        }

        public TextView getClientLabel() {
            return this.clientLabel;
        }

        public TextView getTimeView() {
            return this.timeView;
        }

        public TextView getPaidLabel() {
            return this.paidLabel;
        }
    }
}