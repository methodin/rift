package com.coetre.rift.utils;

import android.content.Context;
import android.widget.Toast;

public class Alert {
	/**
	 * Shows an alert message
	 * @param context
	 * @param text
	 */
	public static void show(Context context, String text) {
		(Toast.makeText(context, text, Toast.LENGTH_LONG))
				.show();
	}

    /**
     * Shows an alert message based on a string id
     * @param context
     * @param id
     */
    public static void show(Context context, int id) {
        String data = context.getResources().getString(id);
        (Toast.makeText(context, data, Toast.LENGTH_LONG))
                .show();
    }
}