package com.coetre.rift.utils;

import android.app.backup.*;
import android.os.Environment;
import android.os.ParcelFileDescriptor;

import java.io.*;

public class RiftBackupAgent extends BackupAgentHelper {
    private static JsonDataManager manager = new JsonDataManager(JsonDataManager.AUTOMATED_FILE_NAMEE);

    @Override
    public void onCreate(){
        FileBackupHelper rift = new FileBackupHelper(this, JsonDataManager.FILE_NAME);
        addHelper("rift", rift);
    }

    @Override
    public void onBackup(ParcelFileDescriptor oldState, BackupDataOutput data, ParcelFileDescriptor newState) throws IOException {
        manager.backup();
        super.onBackup(oldState, data, newState);
    }

    @Override
    public void onRestore(BackupDataInput data, int appVersionCode, ParcelFileDescriptor newState) throws IOException {
        super.onRestore(data, appVersionCode, newState);
        manager.restore();
    }

    @Override
    public File getFilesDir(){
        String directory = Environment.getExternalStorageDirectory() + "/Rift/";
        return new File(directory);
    }
}