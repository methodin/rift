package com.coetre.rift.utils;

import android.database.sqlite.SQLiteConstraintException;
import android.os.Environment;
import android.util.JsonReader;
import android.util.JsonToken;
import android.util.JsonWriter;
import com.activeandroid.query.Select;
import com.coetre.rift.entities.Invoice;
import com.coetre.rift.entities.Task;
import com.coetre.rift.entities.Client;
import com.coetre.rift.entities.InvoiceClient;
import com.coetre.rift.entities.InvoiceRecord;
import com.coetre.rift.entities.TaskLog;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class JsonDataManager {
    public static final String AUTOMATED_FILE_NAMEE = "automated-backup.json";
    public static final String FILE_NAME = "backup.json";
    public static final String DIRECTORY = "Rift";

    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    private String filename = FILE_NAME;

    public JsonDataManager() {

    }

    public JsonDataManager(String filename) {
        this.filename = filename;
    }

    /**
     * Backup the database to a file
     *
     * @return
     * @throws IOException
     */
    public boolean backup() {
        try {
            FileOutputStream out = new FileOutputStream(getFileDirectory() + filename);
            JsonWriter writer = new JsonWriter(new OutputStreamWriter(out, "UTF-8"));
            writer.setIndent("  ");
            writer.beginObject();

            backupClient(writer);
            backupInvoice(writer);
            backupInvoiceClient(writer);
            backupTask(writer);
            backupInvoiceRecord(writer);
            backupTaskLog(writer);

            writer.endObject();
            writer.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Restore the backed up file
     *
     * @return
     */
    public int restore() {
        try {
            FileInputStream in = new FileInputStream(getFileDirectory() + filename);
            JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));

            int count = 0;
            try {
                reader.beginObject();
                while (reader.hasNext()) {
                    String name = reader.nextName();
                    if (name.equals("Client")) {
                        count += restoreClient(reader);
                    } else if (name.equals("Invoice")) {
                        count += restoreInvoice(reader);
                    } else if (name.equals("InvoiceClient")) {
                        count += restoreInvoiceClient(reader);
                    } else if (name.equals("InvoiceRecord")) {
                        count += restoreInvoiceRecord(reader);
                    } else if (name.equals("Task")) {
                        count += restoreTask(reader);
                    } else if (name.equals("TaskLog")) {
                        count += restoreTaskLog(reader);
                    }
                }
                reader.endObject();
            } finally {
                reader.close();
            }

            return count;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Builds the directory for teh backup file
     *
     * @return
     * @throws IOException
     */
    public static String getFileDirectory() throws IOException {
        String directory = Environment.getExternalStorageDirectory() + "/" + DIRECTORY + "/";
        File folder = new File(directory);
        if (!folder.exists()) {
            if (!folder.mkdir()) {
                throw new IOException("Cannot create directory " + directory);
            }
        }
        return directory;
    }

    /**
     * Inserts a record into the DB
     *
     * @param table
     * @param reader
     * @param map
     * @return int
     */
    private int executeInsert(String table, JsonReader reader, HashMap<String, String> map) {
        ArrayList<Object> replacements = new ArrayList<Object>();
        String keyList = "";
        String valueList = "";

        try {
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (map.containsKey(name)) {
                    String type = map.get(name);
                    keyList += (keyList.equals("") ? "" : ",") + name;
                    if (reader.peek() == JsonToken.NULL) {
                        valueList += (valueList.equals("") ? "" : ",") + "NULL";
                        reader.skipValue();
                    } else {
                        valueList += (valueList.equals("") ? "" : ",") + "?";
                        try {
                            if (type.equals("Integer")) {
                                replacements.add(reader.nextInt());
                            } else if (type.equals("Long")) {
                                replacements.add(reader.nextLong());
                            } else if (type.equals("Boolean")) {
                                replacements.add(reader.nextBoolean());
                            } else if (type.equals("Date")) {
                                replacements.add(formatter.parse(reader.nextString()));
                            } else if (type.equals("Double")) {
                                replacements.add(reader.nextDouble());
                            } else {
                                replacements.add(reader.nextString());
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            com.activeandroid.util.SQLiteUtils.execSql(
                    "INSERT INTO " + table + " (" + keyList + ") VALUES (" + valueList + ")",
                    replacements.toArray());
            return 1;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLiteConstraintException e) {
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * Backup the Client entities
     *
     * @param writer
     */
    private void backupClient(JsonWriter writer) throws IOException {
        writer.name("Client");
        writer.beginArray();

        // Fetch all clients
        final List<Client> rows = new Select().from(Client.class).orderBy("Id ASC").execute();
        for (Client row : rows) {
            writer.beginObject();
            writer.name("Id").value(row.getId());
            writer.name("Name").value(row.getName());
            writer.name("Color").value(row.getColor());
            writer.name("Rate").value(row.getRate());
            writer.name("Deleted").value(row.isDeleted());
            writer.endObject();
        }

        writer.endArray();
    }

    /**
     * Restore the Client entities
     *
     * @param reader
     */
    private int restoreClient(JsonReader reader) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Id", "Long");
        map.put("Name", "String");
        map.put("Color", "String");
        map.put("Rate", "Integer");
        map.put("Deleted", "Boolean");
        int count = 0;

        try {
            reader.beginArray();
            while (reader.hasNext()) {
                reader.beginObject();
                count += executeInsert("Client", reader, map);
                reader.endObject();
            }
            reader.endArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * Backup the Invoice entities
     *
     * @param writer
     */
    private void backupInvoice(JsonWriter writer) throws IOException {
        writer.name("Invoice");
        writer.beginArray();

        // Fetch all clients
        final List<Invoice> rows = new Select().from(Invoice.class).orderBy("Id ASC").execute();
        for (Invoice row : rows) {
            writer.beginObject();
            writer.name("Id").value(row.getId());
            writer.name("Date").value(formatter.format(row.getDate()));
            writer.name("Seconds").value(row.getSeconds());
            writer.name("Total").value(row.getTotal());
            writer.endObject();
        }

        writer.endArray();
    }

    /**
     * Restore the Invoice entities
     *
     * @param reader
     */
    private int restoreInvoice(JsonReader reader) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Id", "Long");
        map.put("Date", "Date");
        map.put("Seconds", "Integer");
        map.put("Total", "Double");
        int count = 0;

        try {
            reader.beginArray();
            while (reader.hasNext()) {
                reader.beginObject();
                count += executeInsert("Invoice", reader, map);
                reader.endObject();
            }
            reader.endArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * Backup the InvoiceClient entities
     *
     * @param writer
     */
    private void backupInvoiceClient(JsonWriter writer) throws IOException {
        writer.name("InvoiceClient");
        writer.beginArray();

        // Fetch all clients
        final List<InvoiceClient> rows = new Select().from(InvoiceClient.class).orderBy("Id ASC").execute();
        for (InvoiceClient row : rows) {
            writer.beginObject();
            writer.name("Id").value(row.getId());
            writer.name("Invoice").value(row.getInvoice().getId());
            writer.name("Seconds").value(row.getSeconds());
            writer.name("Client").value(row.getClient().getId());
            writer.name("Rate").value(row.getRate());
            writer.name("Total").value(row.getTotal());
            writer.endObject();
        }

        writer.endArray();
    }

    /**
     * Restore the InvoiceClient entities
     *
     * @param reader
     */
    private int restoreInvoiceClient(JsonReader reader) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Id", "Long");
        map.put("Invoice", "Long");
        map.put("Seconds", "Integer");
        map.put("Client", "Long");
        map.put("Rate", "Integer");
        map.put("Total", "Double");
        int count = 0;

        try {
            reader.beginArray();
            while (reader.hasNext()) {
                reader.beginObject();
                count += executeInsert("InvoiceClient", reader, map);
                reader.endObject();
            }
            reader.endArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * Backup the InvoiceRecord entities
     *
     * @param writer
     */
    private void backupInvoiceRecord(JsonWriter writer) throws IOException {
        writer.name("InvoiceRecord");
        writer.beginArray();

        // Fetch all clients
        final List<InvoiceRecord> rows = new Select().from(InvoiceRecord.class).orderBy("Id ASC").execute();
        for (InvoiceRecord row : rows) {
            writer.beginObject();
            writer.name("Id").value(row.getId());
            writer.name("InvoiceClient").value(row.getInvoiceClient().getId());
            writer.name("Seconds").value(row.getSeconds());
            writer.name("Task").value(row.getTask().getId());
            writer.name("Total").value(row.getTotal());
            writer.name("Parent").value(row.getParent() == null ? null : row.getParent().getId());
            writer.endObject();
        }

        writer.endArray();
    }

    /**
     * Restore the InvoiceRecord entities
     *
     * @param reader
     */
    private int restoreInvoiceRecord(JsonReader reader) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Id", "Long");
        map.put("InvoiceClient", "Long");
        map.put("Seconds", "Integer");
        map.put("Task", "Long");
        map.put("Total", "Double");
        map.put("Parent", "Long");
        int count = 0;

        try {
            reader.beginArray();
            while (reader.hasNext()) {
                reader.beginObject();
                count += executeInsert("InvoiceRecord", reader, map);
                reader.endObject();
            }
            reader.endArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * Backup the Task entities
     *
     * @param writer
     */
    private void backupTask(JsonWriter writer) throws IOException {
        writer.name("Task");
        writer.beginArray();

        // Fetch all clients
        final List<Task> rows = new Select().from(Task.class).orderBy("Id ASC").execute();
        for (Task row : rows) {
            writer.beginObject();
            writer.name("Id").value(row.getId());
            writer.name("Seconds").value(row.getSeconds());
            writer.name("Name").value(row.getName());
            writer.name("Parent").value(row.getParent() == null ? null : row.getParent().getId());
            writer.name("Date").value(row.getDate() == null ? null : formatter.format(row.getDate()));
            writer.name("Client").value(row.getClient() == null ? null : row.getClient().getId());
            writer.name("Deleted").value(row.getDeleted());
            writer.endObject();
        }

        writer.endArray();
    }

    /**
     * Restore the Task entities
     *
     * @param reader
     */
    private int restoreTask(JsonReader reader) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Id", "Long");
        map.put("Seconds", "Integer");
        map.put("Name", "String");
        map.put("Parent", "Long");
        map.put("Date", "Date");
        map.put("Client", "Long");
        map.put("Deleted", "Boolean");
        int count = 0;

        try {
            reader.beginArray();
            while (reader.hasNext()) {
                reader.beginObject();
                count += executeInsert("Task", reader, map);
                reader.endObject();
            }
            reader.endArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * Backup the TaskLog entities
     *
     * @param writer
     */
    private void backupTaskLog(JsonWriter writer) throws IOException {
        writer.name("TaskLog");
        writer.beginArray();

        // Fetch all clients
        final List<TaskLog> rows = new Select().from(TaskLog.class).orderBy("Id ASC").execute();
        for (TaskLog row : rows) {
            writer.beginObject();
            writer.name("Id").value(row.getId());
            writer.name("Task").value(row.getTask().getId());
            writer.name("StartDate").value(formatter.format(row.getStartDate()));
            writer.name("EndDate").value(formatter.format(row.getEndDate()));
            writer.name("Seconds").value(row.getSeconds());
            writer.name("InvoiceRecord").value(row.getInvoiceRecord() == null ? null : row.getInvoiceRecord().getId());
            writer.endObject();
        }

        writer.endArray();
    }

    /**
     * Restore the TaskLog entities
     *
     * @param reader
     */
    private int restoreTaskLog(JsonReader reader) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Id", "Long");
        map.put("Task", "Long");
        map.put("StartDate", "Date");
        map.put("EndDate", "Date");
        map.put("Seconds", "Integer");
        map.put("InvoiceRecord", "Long");
        int count = 0;

        try {
            reader.beginArray();
            while (reader.hasNext()) {
                reader.beginObject();
                count += executeInsert("TaskLog", reader, map);
                reader.endObject();
            }
            reader.endArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }
}