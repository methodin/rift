package com.coetre.rift.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import com.activeandroid.query.Select;
import com.coetre.rift.MainActivity;
import com.coetre.rift.R;
import com.coetre.rift.entities.EntityChangeManager;
import com.coetre.rift.entities.EntityManager;
import com.coetre.rift.entities.Task;
import com.coetre.rift.loaders.TaskLoader;

public class BackupReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(final Context context, Intent intent) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean backupEnabled = prefs.getBoolean(Config.PREFS_KEY_BACKUP_ENABLED, true);
        if (backupEnabled) {
            new JsonDataManager(JsonDataManager.AUTOMATED_FILE_NAMEE).backup();
        }
    }
}