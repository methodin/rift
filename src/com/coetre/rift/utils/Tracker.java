package com.coetre.rift.utils;

import com.google.analytics.tracking.android.EasyTracker;

public class Tracker {
    /**
     * Tracking values
     */
    public final static String CATEGORY_TASK = "task";
    public final static String CATEGORY_CLIENT = "client";
    public final static String CATEGORY_INVOICE = "invoice";
    public final static String CATEGORY_LOG = "log";
    public final static String CATEGORY_SETTINGS = "settings";

    public final static String ACTION_CREATE = "create";
    public final static String ACTION_CREATE_SUBTASK = "create_subtask";
    public final static String ACTION_DELETE = "delete";
    public final static String ACTION_EDIT = "edit";
    public final static String ACTION_MOVE_TIME = "move_time";
    public final static String ACTION_SHOW_SUBTASK_OPTIONS = "show_subtask_options";
    public final static String ACTION_SHOW_TASK_OPTIONS = "show_task_options";
    public final static String ACTION_TIMER = "toggle_timer";
    public final static String ACTION_SHOW_LOG_ACTIONS = "show_log_actions";
    public final static String ACTION_SORT = "sort";
    public final static String ACTION_VIEW = "view";
    public final static String ACTION_CLICK = "click";
    public final static String ACTION_TIME = "time";

    public final static String ACTION_STOP = "stop";
    public final static String ACTION_BUTTON_PRESS = "button_press";

    public final static String ACTION_SETTINGS = "settings";
    public final static String ACTION_CREATE_INVOICE = "create_invoice";
    public final static String ACTION_VIEW_INVOICE = "view_invoice";
    public final static String ACTION_CLIENT = "client";
    public final static String ACTION_LOG = "log";
    public final static String ACTION_TASK = "task";

    public static void trackClient(String action, String label) {
        trackClient(action, label, 0L);
    }
    public static void trackClient(String action, String label, long value) {
        track(CATEGORY_CLIENT, action, label, value);
    }

    public static void trackInvoice(String action, String label) {
        trackInvoice(action, label, 0L);
    }
    public static void trackInvoice(String action, String label, long value) {
        track(CATEGORY_INVOICE, action, label, value);
    }

    public static void trackLog(String action, String label) {
        trackLog(action, label, 0L);
    }
    public static void trackLog(String action, String label, long value) {
        track(CATEGORY_LOG, action, label, value);
    }

    public static void trackTask(String action, String label) {
        trackTask(action, label, 0L);
    }
    public static void trackTask(String action, String label, long value) {
        track(CATEGORY_TASK, action, label, value);
    }

    public static void trackSettings(String action, String label) {
        trackSettings(action, label, 0L);
    }
    public static void trackSettings(String action, String label, long value) {
        track(CATEGORY_SETTINGS, action, label, value);
    }

    public static void track(String category, String action, String label, long value) {
        EasyTracker.getTracker().sendEvent(category, action, label, value);
    }
}