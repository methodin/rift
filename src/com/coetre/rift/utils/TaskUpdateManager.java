package com.coetre.rift.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import com.activeandroid.query.Select;
import com.coetre.rift.entities.*;
import com.google.analytics.tracking.android.EasyTracker;

import java.util.Calendar;

public class TaskUpdateManager {
    private final static long TYPE_START = 1;
    private final static long TYPE_STOP = 2;
    private static long update = 0;

    /**
	 * Add a task to the update list
	 * 
	 * @param context
	 * @param task
	 */
	public static boolean update(Context context, Task task) {
        long id = task.getId();
		final SharedPreferences prefs = context.getSharedPreferences(Config.PREFS_NAME,
				Context.MODE_PRIVATE);
		final Editor editor = prefs.edit();
        update = prefs.getLong(Config.PREFS_KEY_ACTIVE_TASK, 0);

		boolean added = false;
		if (id != update) {
            if(update != 0) {
                new TaskUpdater().execute(update, TYPE_STOP);
            }
            new TaskUpdater().execute(id, TYPE_START);
			update = id;
			added = true;
            Tracker.trackTask(Tracker.ACTION_TIMER, "TaskUpdateManager.start");
		} else {
            new TaskUpdater().execute(id, TYPE_STOP);
            update = 0;
            Tracker.trackTask(Tracker.ACTION_TIMER, "TaskUpdateManager.stop");
		}

		editor.putLong(Config.PREFS_KEY_ACTIVE_TASK, update);
		editor.apply();
		
		return added;
	}

    public static class TaskUpdater extends AsyncTask<Long, Void, Task> {
        @Override
        protected void onPostExecute(Task task) {
            if(task != null) {
                EntityChangeManager.notifyListeners(task);
                TaskActiveChangeManager.change();
                GlobalEntityChangeManager.notifyListeners(Task.class, GlobalEntityChangeManager.ACTION_CHANGE);
            }
        }

        @Override
        protected Task doInBackground(Long... params) {
            Task task = new Select().from(Task.class).where("Id = ?", params[0]).executeSingle();
            EntityManager<Task> entityManager = EntityManager.getInstance(Task.class);
            task = entityManager.add(task);

            Boolean updated = false;
            if(params[1].equals(TYPE_START)) {
                task.setDate(Calendar.getInstance().getTime());
                updated = true;
            } else {
                int seconds = task.getElapsedSeconds();
                if(seconds > 0) {
                    Tracker.trackTask(Tracker.ACTION_TIME, "TaskUpdateManager", (long)seconds);
                    new TaskLog(task, task.getDate(), Calendar.getInstance().getTime(), seconds).save();
                    task.incrementSeconds(seconds);
                    updated = true;
                }
                task.setDate(null);
            }
            task.save();

            if(updated) {
                return task;
            }

            return null;
        }
    }
}