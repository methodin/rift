package com.coetre.rift.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import com.activeandroid.query.Select;
import com.coetre.rift.MainActivity;
import com.coetre.rift.R;
import com.coetre.rift.entities.EntityChangeManager;
import com.coetre.rift.entities.EntityManager;
import com.coetre.rift.entities.Task;
import com.coetre.rift.loaders.TaskLoader;

public class TaskReceiver extends BroadcastReceiver {
    private final static int NOTIFICATION_ID = 1;
    private EntityManager<Task> entityManager = EntityManager.getInstance(Task.class);
    private Task task;

    @Override
    public void onReceive(final Context context, Intent intent) {
        SharedPreferences prefs = context.getSharedPreferences(Config.PREFS_NAME,
                Context.MODE_PRIVATE);
        if (prefs.contains(Config.PREFS_KEY_ACTIVE_TASK)) {
            Long taskId = prefs.getLong(Config.PREFS_KEY_ACTIVE_TASK, 0);
            if (taskId > 0) {
                task = entityManager.get(taskId);
                if (task != null) {
                    EntityChangeManager.notifyListeners(task);
                }
            }
        }
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean showNotification = prefs.getBoolean(Config.PREFS_NOTIFICATION,true);
        if(showNotification) {
            new TaskLoader(new TaskLoader.OnTaskLoaderCompleted() {
                @Override
                public void onTaskLoaderCompleted(Task task) {
                    showNotification(task, context);
                }
            }).execute(new Select().from(Task.class).where("Date IS NOT NULL"));
        }
    }

    private void showNotification(Task task, Context context) {
        NotificationManager notificationManager;
        if (task != null) {
            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(context)
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setContentTitle(task.getName())
                            .setContentText(context.getResources().getString(R.string.loggedTime)+task.getFormattedTime())
                            .setWhen(task.getDate().getTime())
                            .setContentInfo(task.getFormattedRate());
            // Creates an explicit intent for an Activity in your app
            Intent resultIntent = new Intent(context, MainActivity.class);
            resultIntent.putExtra(MainActivity.DATA_NOTIFICATION, true);

            // The stack builder object will contain an artificial back stack for the
            // started Activity.
            // This ensures that navigating backward from the Activity leads out of
            // your application to the Home screen.
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            // Adds the Intent that starts the Activity to the top of the stack
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(
                            0, PendingIntent.FLAG_UPDATE_CURRENT
                    );
            builder.setContentIntent(resultPendingIntent);

            Intent intent = new Intent(context, StopReceiver.class);
            intent.putExtra(StopReceiver.DATA_TASK_ID, task.getId());
            PendingIntent resultDeleteIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
            builder.setDeleteIntent(resultDeleteIntent);

            notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(NOTIFICATION_ID, builder.build());
        } else {
            notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(NOTIFICATION_ID);
        }
    }
}