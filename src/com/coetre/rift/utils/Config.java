package com.coetre.rift.utils;

import android.graphics.Typeface;

public class Config {
	public static final int SORT_NAME_ASC = 1;
	public static final int SORT_NAME_DESC = 2;
	public static final int SORT_TIME_ASC = 3;
	public static final int SORT_TIME_DESC = 4;
	public static final int SORT_DATE_ASC = 5;
	public static final int SORT_DATE_DESC = 6;
	
	public final static int MODE_TASK = 0;
	public final static int MODE_ACTIVE = 1;
	public final static int MODE_CLIENTS = 2;
    public final static int MODE_INVOICE = 3;
    public final static int MODE_SETTINGS = 4;
	
	/**
	 * The shared prefs editor name
	 */
	public final static String PREFS_NAME = "com.coetre.rift";
    public final static String PACKAGE_NAME = "com.coetre.rift";
	
	/**
	 * The shared prefs data access keys
	 */
	public final static String PREFS_KEY_ACTIVE_TASK = "ActiveTask";
	public final static String PREFS_KEY_MODE = "Mode";
	public final static String PREFS_KEY_SORT = "Sort";
    public final static String PREFS_KEY_BACKUP_ENABLED = "pref_backup_enabled";
    public final static String PREFS_TIMER = "pref_timer";
    public final static String PREFS_NOTIFICATION = "pref_notification_enabled";
	
	/**
	 * Global options
	 */
	public static int sort = SORT_DATE_ASC;
	public static int mode = MODE_TASK;

    public static Typeface thin = Typeface.create("sans-serif-light", Typeface.NORMAL);
}