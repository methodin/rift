package com.coetre.rift.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.activeandroid.query.Select;
import com.coetre.rift.R;
import com.coetre.rift.entities.EntityManager;
import com.coetre.rift.entities.Task;

public class StopReceiver extends BroadcastReceiver {
    public static final String DATA_TASK_ID = "taskId";

    @Override
    public void onReceive(final Context context, Intent intent) {
        if(intent.getExtras().containsKey(DATA_TASK_ID)) {
            Long id = intent.getLongExtra(DATA_TASK_ID, 0);
            Task task = new Select().from(Task.class).where("Id = ?", id).executeSingle();
            EntityManager<Task> entityManager = EntityManager.getInstance(Task.class);
            task = entityManager.add(task);
            TaskUpdateManager.update(context, task);
            Toast.makeText(context, task.getName()+context.getResources().getString(R.string.successfullyStopped), Toast.LENGTH_LONG).show();
        }
    }
}