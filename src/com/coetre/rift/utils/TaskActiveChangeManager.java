package com.coetre.rift.utils;


import java.util.concurrent.CopyOnWriteArrayList;

public class TaskActiveChangeManager {
	private static CopyOnWriteArrayList<TaskActiveChangeListener> listeners = new CopyOnWriteArrayList<TaskActiveChangeListener>();
	
	public static void add(TaskActiveChangeListener listener) {
        listeners.add(listener);
	}
	
	public static void change() {
		if(listeners.size() > 0) {
            for(TaskActiveChangeListener listener : listeners) {
			    listener.change();
            }
		}
	}
	
	public interface TaskActiveChangeListener {
		public void change();
	}
}