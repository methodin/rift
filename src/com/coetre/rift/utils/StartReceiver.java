package com.coetre.rift.utils;

import java.util.Calendar;
import java.util.Locale;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class StartReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent incomingIintent) {
		updateIntent(context);
	}

	/**
	 * Update the next context
	 * @param context
	 */
	public static void updateIntent(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String timerString = prefs.getString(Config.PREFS_TIMER,"1");
        int timer = Integer.valueOf(timerString);
        if(timer > 60) {
            timer = 60;
        }
        if(timer < 0) {
            timer = 1;
        }

		Intent intent = new Intent(context, TaskReceiver.class);
		PendingIntent sender = PendingIntent.getBroadcast(context, 2,
				intent, PendingIntent.FLAG_CANCEL_CURRENT);
		AlarmManager am = (AlarmManager) context
				.getSystemService(Service.ALARM_SERVICE);

		// Set an alarm to start at the top of the next hour
		am.cancel(sender);
		Calendar c = Calendar.getInstance(Locale.getDefault());
		c.set(Calendar.SECOND, c.get(Calendar.SECOND)+1);
		c.set(Calendar.MILLISECOND, 0);
		am.setRepeating(AlarmManager.RTC, Calendar.getInstance().getTimeInMillis(),
				timer*1000, sender);

        intent = new Intent(context, BackupReceiver.class);
        sender = PendingIntent.getBroadcast(context, 3,
                intent, PendingIntent.FLAG_CANCEL_CURRENT);

        // Set an alarm to start at the top of the next hour
        am.cancel(sender);
        c.set(Calendar.HOUR_OF_DAY, 2);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        am.setRepeating(AlarmManager.RTC, c.getTimeInMillis(), AlarmManager.INTERVAL_DAY, sender);
	}
}