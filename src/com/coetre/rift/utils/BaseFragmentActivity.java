package com.coetre.rift.utils;

import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import com.coetre.rift.R;
import com.coetre.rift.actions.TaskActions;
import com.google.analytics.tracking.android.EasyTracker;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BaseFragmentActivity extends FragmentActivity {
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        switch (Config.sort) {
            case Config.SORT_DATE_ASC:
                menu.findItem(R.id.actionSortDate).setChecked(true);
                break;
            case Config.SORT_DATE_DESC:
                menu.findItem(R.id.actionSortDateDesc).setChecked(true);
                break;
            case Config.SORT_TIME_ASC:
                menu.findItem(R.id.actionSortTime).setChecked(true);
                break;
            case Config.SORT_TIME_DESC:
                menu.findItem(R.id.actionSortTimeDesc).setChecked(true);
                break;
            case Config.SORT_NAME_ASC:
                menu.findItem(R.id.actionSortName).setChecked(true);
                break;
            case Config.SORT_NAME_DESC:
                menu.findItem(R.id.actionSortNameDesc).setChecked(true);
                break;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.isCheckable()) {
            item.setChecked(true);
        }
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.actionAdd:
                TaskActions.add(this);
                return true;
            case R.id.actionSortName:
                sortClicked(Config.SORT_NAME_ASC);
                return true;
            case R.id.actionSortNameDesc:
                sortClicked(Config.SORT_NAME_DESC);
                return true;
            case R.id.actionSortTime:
                sortClicked(Config.SORT_TIME_ASC);
                return true;
            case R.id.actionSortTimeDesc:
                sortClicked(Config.SORT_TIME_DESC);
                return true;
            case R.id.actionSortDate:
                sortClicked(Config.SORT_DATE_ASC);
                return true;
            case R.id.actionSortDateDesc:
                sortClicked(Config.SORT_DATE_DESC);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void sortClicked(int sort) {
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance().activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance().activityStop(this);
    }
}