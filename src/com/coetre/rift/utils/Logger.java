package com.coetre.rift.utils;

import android.util.Log;

public class Logger {
	public static final String APP_KEY = "Rift";
	public static void d(String d) {
		Log.d(APP_KEY, d);
	}
	public static void d(Long d) {
		Log.d(APP_KEY, Long.toString(d));
	}
}