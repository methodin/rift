package com.coetre.rift;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.activeandroid.query.Select;
import com.coetre.rift.entities.EntityManager;
import com.coetre.rift.entities.TaskLog;
import com.coetre.rift.actions.LogActions;
import com.coetre.rift.actions.LogActions.LogCallback;
import com.google.analytics.tracking.android.EasyTracker;

public class LogOptionsActivity extends ListActivity {
	public static final String DATA_SUBTASK_ID = "subtaskId";
	private String[] items;
	private TaskLog log;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(DATA_SUBTASK_ID)) {
			long id = getIntent().getExtras().getLong(DATA_SUBTASK_ID, 0);
			log = new Select().from(TaskLog.class).where("Id = ?", id).executeSingle();
			log = (TaskLog) EntityManager.getInstance(TaskLog.class).get(log);
		}

		items = getResources().getStringArray(R.array.logOptions);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1,
				items);
		setListAdapter(adapter);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		String selected = items[position];
		LogCallback callback = new LogCallback(){
			@Override
			public void run() {
				finish();
			}};
		if(selected.equals(getResources().getString(R.string.logDelete))) {
			LogActions.delete(this, log, callback);
		} else if(selected.equals(getResources().getString(R.string.logEdit))) {
			LogActions.edit(this, log, callback);
		} else if(selected.equals(getResources().getString(R.string.logReassign))) {
			LogActions.moveTime(this, log, callback);
		} 
		super.onListItemClick(l, v, position, id);
	}

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance().activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance().activityStop(this);
    }
}