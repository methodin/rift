package com.coetre.rift;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Spinner;
import com.coetre.rift.adapters.ColorAdapter;
import com.coetre.rift.entities.Client;
import com.coetre.rift.entities.GlobalEntityChangeManager;
import com.coetre.rift.utils.Alert;
import com.google.analytics.tracking.android.EasyTracker;

import java.util.ArrayList;
import java.util.Collections;

public class AddClientActivity extends Activity implements OnClickListener {
    private Spinner colorList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_client);

		findViewById(R.id.cancelButton).setOnClickListener(this);
		findViewById(R.id.addButton).setOnClickListener(this);

        colorList = (Spinner) findViewById(R.id.colorList);
        ArrayList<String> colors = new ArrayList<String>();
        Collections.addAll(colors, getResources().getStringArray(R.array.colors));
        ColorAdapter adapter = new ColorAdapter(getBaseContext(), colors);
        colorList.setAdapter(adapter);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.cancelButton:
			finish();
			break;
		case R.id.addButton:
			String name = ((EditText) findViewById(R.id.clientName)).getText().toString();
            String color = (String)colorList.getSelectedItem();
            String rate = ((EditText) findViewById(R.id.rate)).getText().toString();

            boolean validRate = true;
            int actualRate = 0;
            try {
                actualRate = Integer.parseInt(rate);
            } catch (NumberFormatException e) {
                validRate = false;
            }

			// Form validation
			if (name.equals("")) {
				Alert.show(getApplicationContext(), getResources().getString(R.string.clientNameEmpty));
            } else if(!validRate) {
                Alert.show(getApplicationContext(), getResources().getString(R.string.clientRateEmpty));
			} else {
				new Client(name, color, actualRate).save();
                Alert.show(getApplicationContext(), getResources().getString(R.string.clientSaveSuccess));
				GlobalEntityChangeManager.notifyListeners(Client.class, GlobalEntityChangeManager.ACTION_NEW);
				setResult(RESULT_OK, getIntent());
				finish();
			}
			break;
		}
	}

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance().activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance().activityStop(this);
    }
}