package com.coetre.rift.loaders;

import android.os.AsyncTask;
import android.view.View;
import android.widget.AbsListView;
import android.widget.TextView;
import com.activeandroid.query.From;
import com.coetre.rift.actions.TaskActions;
import com.coetre.rift.adapters.TaskCardAdapter;
import com.coetre.rift.entities.EntityManager;
import com.coetre.rift.entities.Task;
import com.coetre.rift.utils.Config;

import java.lang.ref.WeakReference;
import java.util.List;

public class TaskListLoader extends AsyncTask<From, Void, List<Task>> {
    private final WeakReference<AbsListView> referenceGridView;
    private final WeakReference<View> referenceEmptyView;

    public TaskListLoader(AbsListView gridView, View emptyView) {
        referenceGridView = new WeakReference<AbsListView>(gridView);
        referenceEmptyView = new WeakReference<View>(emptyView);
    }

    @Override
    protected List<Task> doInBackground(From... params) {
        return params[0].execute();
    }

    /**
     * Set the ListView items in the UI thread
     */
    @Override
    protected void onPostExecute(List<Task> items) {
        final AbsListView gridView = referenceGridView.get();
        final View emptyView = referenceEmptyView.get();
        if(gridView != null) {
            TaskCardAdapter adapter = (TaskCardAdapter) gridView.getAdapter();
            if(items.size() > 0) {
                EntityManager<Task> entityManager = EntityManager.getInstance(Task.class);
                items = entityManager.filter(items);
                TaskActions.sort(Config.sort, items);
                adapter.setTasks(items);
                emptyView.setVisibility(View.GONE);
            } else if(emptyView != null) {
                emptyView.setVisibility(View.VISIBLE);
                adapter.setTasks(null);
            }
        }
    }
}