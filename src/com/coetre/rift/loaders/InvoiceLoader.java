package com.coetre.rift.loaders;

import android.os.AsyncTask;
import com.activeandroid.query.From;
import com.coetre.rift.entities.Invoice;

public class InvoiceLoader extends AsyncTask<From, Void, Invoice> {
    final private OnInvoiceLoaderCompleted callback;

    public InvoiceLoader(OnInvoiceLoaderCompleted callback) {
        this.callback = callback;
    }

    @Override
    protected Invoice doInBackground(From... params) {
        return params[0].executeSingle();
    }

    /**
     * Set the ListView items in the UI thread
     */
    @Override
    protected void onPostExecute(Invoice item) {
        this.callback.onInvoiceLoaderCompleted(item);
    }

    public interface OnInvoiceLoaderCompleted {
        public void onInvoiceLoaderCompleted(Invoice invoice);
    }
}