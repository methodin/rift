package com.coetre.rift.loaders;

import android.os.AsyncTask;
import com.activeandroid.query.From;
import com.coetre.rift.entities.EntityManager;
import com.coetre.rift.entities.Task;

public class TaskLoader extends AsyncTask<From, Void, Task> {
    final private OnTaskLoaderCompleted callback;

    public TaskLoader(OnTaskLoaderCompleted callback) {
        this.callback = callback;
    }

    @Override
    protected Task doInBackground(From... params) {
        return params[0].executeSingle();
    }

    /**
     * Set the ListView items in the UI thread
     */
    @Override
    protected void onPostExecute(Task item) {
        if(item != null) {
            EntityManager<Task> entityManager = EntityManager.getInstance(Task.class);
            item = entityManager.add(item);
            item.getTasks();
        }
        this.callback.onTaskLoaderCompleted(item);
    }

    public interface OnTaskLoaderCompleted {
        public void onTaskLoaderCompleted(Task task);
    }
}