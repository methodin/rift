package com.coetre.rift.loaders;

import android.os.AsyncTask;
import com.activeandroid.query.From;
import com.coetre.rift.entities.Client;
import com.coetre.rift.entities.EntityManager;

public class ClientLoader extends AsyncTask<From, Void, Client> {
    final private OnClientLoaderCompleted callback;

    public ClientLoader(OnClientLoaderCompleted callback) {
        this.callback = callback;
    }

    @Override
    protected Client doInBackground(From... params) {
        return params[0].executeSingle();
    }

    /**
     * Set the ListView items in the UI thread
     */
    @Override
    protected void onPostExecute(Client item) {
        EntityManager<Client> entityManager = EntityManager.getInstance(Client.class);
        item = entityManager.add(item);
        this.callback.onClientLoaderCompleted(item);
    }

    public interface OnClientLoaderCompleted {
        public void onClientLoaderCompleted(Client client);
    }
}