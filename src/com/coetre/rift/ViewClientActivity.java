package com.coetre.rift;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.activeandroid.query.Select;
import com.coetre.rift.actions.ClientActions;
import com.coetre.rift.adapters.TaskCardAdapter;
import com.coetre.rift.entities.*;
import com.coetre.rift.loaders.ClientLoader;
import com.coetre.rift.loaders.TaskListLoader;
import com.google.analytics.tracking.android.EasyTracker;

import java.util.List;

public class ViewClientActivity extends Activity  implements EntityChangeManager.EntityChangeListener {
    public final static String DATA_CLIENT_ID = "clientId";
    private Client client;
    private TextView clientLabelView;
    private TextView emptyView;
    private GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_client);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        clientLabelView = (TextView) findViewById(R.id.clientLabel);
        emptyView = (TextView) findViewById(android.R.id.empty);
        gridView = (GridView) findViewById(R.id.cardList);
        TaskCardAdapter adapter = new TaskCardAdapter(getBaseContext(), R.layout.view_task_card, null);
        gridView.setAdapter(adapter);

        if(getIntent().getExtras() != null&& getIntent().getExtras().containsKey(DATA_CLIENT_ID)) {
            new ClientLoader(new ClientLoader.OnClientLoaderCompleted() {
                @Override
                public void onClientLoaderCompleted(Client client) {
                    ViewClientActivity.this.client = client;
                    update(client);
                    new TaskListLoader(gridView, emptyView).execute(new Select().from(Task.class).where("Client = ?", client.getId()));
                }
            }).execute(new Select().from(Client.class).where("Id = ?", getIntent().getExtras().getLong(DATA_CLIENT_ID)));
        }

        EntityChangeManager.add(Client.class, this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.actionEdit:
                ClientActions.edit(this, client);
                break;
            case R.id.actionDelete:
                ClientActions.delete(this, client, new ClientActions.OnDeleteListener() {
                    @Override
                    public void delete() {
                        finish();
                    }
                });
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.view_client, menu);
        return true;
    }

    /**
     * Update all views with the given task
     */
    public void update(Client obj) {
        if(obj.equals(client)) {
            String extra = "";
            if(obj.getRate() > 0) {
                extra = " - Rate: $"+Integer.toString(obj.getRate())+"/hr";
            }
            clientLabelView.setText(obj.getName()+extra);
            clientLabelView.setBackgroundColor(obj.getColorAsInt());
            clientLabelView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public <T> void change(T obj) {
        update((Client)obj);
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance().activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance().activityStop(this);
    }
}