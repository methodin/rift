package com.coetre.rift;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Spinner;
import com.activeandroid.query.Select;
import com.coetre.rift.adapters.ClientAdapter;
import com.coetre.rift.entities.Client;
import com.coetre.rift.entities.EntityManager;
import com.coetre.rift.entities.GlobalEntityChangeManager;
import com.coetre.rift.entities.Task;
import com.coetre.rift.utils.Alert;
import com.google.analytics.tracking.android.EasyTracker;

import java.util.List;

public class AddTaskActivity extends Activity implements OnClickListener {
	private Spinner list;
	
	/**
	 * Responds to the cancel event
	 */
	private OnClickListener cancelListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			AddTaskActivity.this.finish();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_task);

		findViewById(R.id.cancelButton).setOnClickListener(cancelListener);
        findViewById(R.id.addButton).setOnClickListener(this);
		
		List<Client> clients = new Select().from(Client.class).where("deleted != 1")
				.execute();
		EntityManager<Client> taskManager = EntityManager
				.getInstance(Client.class);
		clients = taskManager.filter(clients);
		ClientAdapter adapter = new ClientAdapter(getBaseContext(), clients, true, ClientAdapter.MODE_SELECT);
		
		list = (Spinner) findViewById(R.id.clientList);
		list.setAdapter(adapter);
	}


	@Override
	public void onClick(View v) {
		String description = ((EditText) findViewById(R.id.taskDescription))
				.getText().toString();

		// Form validation
		if (description.equals("")) {
			Alert.show(getApplicationContext(), getResources().getString(R.string.taskNameEmpty));
		} else {
			Client client = (Client) list.getSelectedItem();
			client = (client != null && client.getId() != null && client.getId() > 0 ? client : null);
			new Task(description, 0, null, client).save();
            Alert.show(getApplicationContext(), getResources().getString(R.string.taskSaveSuccess));
			GlobalEntityChangeManager.notifyListeners(Task.class, GlobalEntityChangeManager.ACTION_NEW);
			setResult(RESULT_OK, getIntent());
			finish();
		}
	}

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance().activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance().activityStop(this);
    }
}