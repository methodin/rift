package com.coetre.rift;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.*;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.activeandroid.query.Select;
import com.coetre.rift.adapters.LogAdapter;
import com.coetre.rift.entities.*;
import com.coetre.rift.entities.GlobalEntityChangeManager.GlobalEntityChangeListener;
import com.coetre.rift.actions.LogActions;
import com.coetre.rift.actions.TaskActions;
import com.google.analytics.tracking.android.EasyTracker;

public class ViewLogActivity extends Activity implements OnItemClickListener, GlobalEntityChangeListener {
	public static final String DATA_TASK_ID = "taskId";
	List<TaskLog> logs;
	ListView listView;
	long id;
	Task task;
    LogAdapter adapter;

    @Override
    protected void onPause() {
        super.onPause();
        GlobalEntityChangeManager.remove(TaskLog.class, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        GlobalEntityChangeManager.add(TaskLog.class, this);
    }

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_log);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		listView = (ListView) findViewById(android.R.id.list);
		if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(DATA_TASK_ID)) {
			id = getIntent().getExtras().getLong(DATA_TASK_ID, 0);
			EntityManager<Task> taskEntityManager = EntityManager.getInstance(Task.class);
			task = new Select().from(Task.class).where("Id = ?", id).executeSingle();
			task = taskEntityManager.get(task);
			logs = new Select().from(TaskLog.class).where("Task = ?", id).orderBy("Id DESC").execute();
			EntityManager<TaskLog> entityManager = EntityManager.getInstance(TaskLog.class);
			logs = entityManager.filter(logs);

			adapter = new LogAdapter(getBaseContext(), logs);
			listView.setAdapter(adapter);
			listView.setOnItemClickListener(this);

			checkForEmpty();
		}

		GlobalEntityChangeManager.add(TaskLog.class, new GlobalEntityChangeListener() {
			@Override
			public void change(int action) {
				EntityManager<TaskLog> entityManager;
				switch (action) {
				case GlobalEntityChangeManager.ACTION_CHANGE:
				case GlobalEntityChangeManager.ACTION_NEW:
				case GlobalEntityChangeManager.ACTION_DELETE:
					logs = new Select().from(TaskLog.class).where("Task = ?", id).orderBy("Id DESC").execute();
					entityManager = EntityManager.getInstance(TaskLog.class);
					logs = entityManager.filter(logs);
					LogAdapter adapter = (LogAdapter) listView.getAdapter();
					adapter.setItems(logs);
					checkForEmpty();
					break;
				}
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;
		case R.id.actionAdd:
			if(task != null) {
				LogActions.add(this, task, null);
			}
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.view_log, menu);
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		TaskActions.showLogActions(getBaseContext(), logs.get(position));
	}
	
	private void checkForEmpty() {
		if (logs.size() == 0) {
			listView.setVisibility(View.GONE);
			findViewById(android.R.id.empty).setVisibility(View.VISIBLE);
		}
	}

    @Override
    public void change(int action) {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance().activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance().activityStop(this);
    }
}