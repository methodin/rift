package com.coetre.rift.actions;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import com.coetre.rift.ClientOptionsActivity;
import com.coetre.rift.EditClientActivity;
import com.coetre.rift.R;
import com.coetre.rift.entities.*;
import com.coetre.rift.utils.Alert;
import com.coetre.rift.utils.Config;
import com.coetre.rift.utils.Logger;
import com.coetre.rift.utils.Tracker;
import com.google.analytics.tracking.android.EasyTracker;

import java.util.Calendar;

import java.util.List;

public class ClientActions {
    /**
     * Attempt to delete the client, only if it has no children or unpaid time
     */
    public static void delete(final Context context, final Client client, final OnDeleteListener listener) {
        new AlertDialog.Builder(context).setTitle(context.getResources().getString(R.string.confirmDeletion)+" - "+client.getName())
                .setMessage(context.getResources().getString(R.string.confirmClientDeletionText))
                .setIcon(R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        List<Task> tasks = client.getTasks();
                        for (Task task : tasks) {
                            task.setClient(null);
                            task.save();
                            EntityChangeManager.notifyListeners(task);
                        }

                        Tracker.trackClient(Tracker.ACTION_DELETE, "ClientActions");
                        Alert.show(context, context.getResources().getString(R.string.clientDeleteSuccess));
                        client.setDeleted(true);
                        client.save();
                        GlobalEntityChangeManager.notifyListeners(Client.class,
                                GlobalEntityChangeManager.ACTION_DELETE);
                        if (listener != null) {
                            listener.delete();
                        }
                    }
                }).setNegativeButton(android.R.string.no, null).show();
    }

    /**
     * Show the edit client activity
     * @param context
     * @param client
     */
    public static void edit(final Context context, final Client client) {
        Tracker.trackClient(Tracker.ACTION_EDIT, "ClientActions");
        Intent intent = new Intent(context, EditClientActivity.class);
        intent.putExtra(EditClientActivity.DATA_CLIENT_ID, client.getId());
        context.startActivity(intent);
    }

    /**
     * Adds a new invoice with the given client lsit
     */
    public static void invoice(final Context context, final List<Client> clients) {
        double total = 0;
        int totalSeconds = 0;
        String clientString = "";
        for(Client client : clients) {
            int seconds = client.getUnpaidSeconds();
            totalSeconds += seconds;
            total += client.getAppliedRate(seconds);
            clientString += (clientString.equals("")?"":", ")+client.getName();
        }

        String text = context.getResources().getString(R.string.confirmInvoiceText);
        text = text.replace("$CLIENTS",clientString);
        text = text.replace("$AMOUNT",String.format("%2.2f", total));
        final Double totalReference = total;
        final Integer totalSecondsReference = totalSeconds;
        if(total > 0 && clients.size() > 0) {
            new AlertDialog.Builder(context).setTitle(context.getResources().getString(R.string.confirmInvoice))
                    .setMessage(text)
                    .setIcon(R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            Invoice invoice = new Invoice(Calendar.getInstance().getTime(), totalSecondsReference, totalReference);
                            invoice.save();
                            for (final Client invoiceClient : clients) {
                                int seconds = 0;
                                double total = 0;
                                int clientSeconds = invoiceClient.getUnpaidSeconds();
                                InvoiceClient invoiceClientRecord = new InvoiceClient(invoice, seconds, invoiceClient, invoiceClient.getRate(), invoiceClient.getAppliedRate(clientSeconds));
                                invoiceClientRecord.save();
                                for (Task task : invoiceClient.getTasks()) {
                                    if(task.getSeconds() > 0) {
                                        int onlyTaskSeconds = task.getSeconds()-task.getChildSeconds();
                                        InvoiceRecord invoiceRecord = new InvoiceRecord(invoiceClientRecord, onlyTaskSeconds, task, invoiceClient.getAppliedRate(onlyTaskSeconds), task.getParent());
                                        invoiceRecord.save();
                                        for (TaskLog log : task.getTaskLogs()) {
                                            seconds += log.getSeconds();
                                            total += invoiceClient.getAppliedRate(log.getSeconds());
                                            log.setInvoiceRecord(invoiceRecord);
                                            log.save();
                                        }
                                        task.setSeconds(0);
                                        task.save();
                                        for(Task subTask : task.getTasks()) {
                                            new InvoiceRecord(invoiceClientRecord, subTask.getSeconds(), subTask,
                                                    invoiceClient.getAppliedRate(subTask.getSeconds()), subTask.getParent()).save();
                                            for (TaskLog log : subTask.getTaskLogs()) {
                                                log.setInvoiceRecord(invoiceRecord);
                                                log.save();
                                            }
                                            subTask.setSeconds(0);
                                            subTask.save();
                                        }
                                    }
                                }
                            }

                            Tracker.trackInvoice(Tracker.ACTION_CREATE, "ClientActions");
                            Alert.show(context, context.getResources().getString(R.string.clientInvoiceSuccess));
                            GlobalEntityChangeManager.notifyListeners(Task.class, GlobalEntityChangeManager.ACTION_CHANGE);
                            GlobalEntityChangeManager.notifyListeners(Client.class, GlobalEntityChangeManager.ACTION_CHANGE);
                            GlobalEntityChangeManager.notifyListeners(Invoice.class, GlobalEntityChangeManager.ACTION_NEW);
                        }
                    }).setNegativeButton(android.R.string.no, null).show();
        } else {
            Alert.show(context, context.getResources().getString(R.string.errorNoClientsInvoice));
        }
    }

    public interface OnDeleteListener {
        public void delete();
    }
}