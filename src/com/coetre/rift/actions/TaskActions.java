package com.coetre.rift.actions;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.coetre.rift.AddSubTaskActivity;
import com.coetre.rift.AddTaskActivity;
import com.coetre.rift.EditTaskActivity;
import com.coetre.rift.LogOptionsActivity;
import com.coetre.rift.R;
import com.coetre.rift.SubTaskOptionsActivity;
import com.coetre.rift.TaskOptionsActivity;
import com.coetre.rift.ViewLogActivity;
import com.coetre.rift.entities.GlobalEntityChangeManager;
import com.coetre.rift.entities.Task;
import com.coetre.rift.entities.TaskLog;
import com.coetre.rift.utils.Alert;
import com.coetre.rift.utils.Config;
import com.coetre.rift.utils.TaskUpdateManager;
import com.coetre.rift.utils.Tracker;
import com.google.analytics.tracking.android.EasyTracker;

public class TaskActions {
	/**
	 * Starts the Edit Task dialog
	 */
	public static void edit(Context context, Task task) {
        Tracker.trackTask(Tracker.ACTION_EDIT, "TaskActions");
		Intent intent = new Intent(context, EditTaskActivity.class);
		intent.putExtra(EditTaskActivity.DATA_TASK_ID, task.getId());
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}

	/**
	 * Starts the Add Task dialog
	 */
	public static void add(Context context) {
        Tracker.trackTask(Tracker.ACTION_CREATE, "TaskActions");
		Intent intent = new Intent(context, AddTaskActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}

	/**
	 * Starts the Add SubTask dialog
	 */
	public static void addSubTask(Context context, Task task) {
        Tracker.trackTask(Tracker.ACTION_CREATE_SUBTASK, "TaskActions");
		Intent intent = new Intent(context, AddSubTaskActivity.class);
		intent.putExtra(AddSubTaskActivity.DATA_TASK_ID, task.getId());
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}

	/**
	 * Shows the SubTask options
	 */
	public static void showSubTaskActions(Context context, Task task) {
        Tracker.trackTask(Tracker.ACTION_SHOW_SUBTASK_OPTIONS, "TaskActions");
		Intent intent = new Intent(context, SubTaskOptionsActivity.class);
		intent.putExtra(SubTaskOptionsActivity.DATA_TASK_ID, task.getId());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}

	/**
	 * Shows the task options
	 */
	public static void showTaskActions(Context context, Task task) {
        Tracker.trackTask(Tracker.ACTION_SHOW_TASK_OPTIONS, "TaskActions");
		Intent intent = new Intent(context, TaskOptionsActivity.class);
		intent.putExtra(SubTaskOptionsActivity.DATA_TASK_ID, task.getId());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}

	/**
	 * Starts a task timer
	 */
	public static void toggleTimer(Context context, Task task) {
        Tracker.trackTask(Tracker.ACTION_TIMER, "TaskActions");
		TaskUpdateManager.update(context, task);
	}

	/**
	 * Sort the list of Task objects
	 * 
	 * @param sort
	 * @param tasks
	 */
	public static void sort(final int sort, List<Task> tasks) {
        Tracker.trackTask(Tracker.ACTION_SORT, "TaskActions", (long)sort);
		final Comparator<Task> comp = new Comparator<Task>() {
			public int compare(Task e1, Task e2) {
				switch (sort) {
				case Config.SORT_NAME_ASC:
					return e1.getName().compareTo(e2.getName());
				case Config.SORT_NAME_DESC:
					return e2.getName().compareTo(e1.getName());
				case Config.SORT_TIME_ASC:
					return e1.getSeconds() > e2.getSeconds() ? 1 : (e1.getSeconds() == e2.getSeconds() ? 0 : -1);
				case Config.SORT_TIME_DESC:
					return e2.getSeconds() > e1.getSeconds() ? 1 : (e1.getSeconds() == e2.getSeconds() ? 0 : -1);
				case Config.SORT_DATE_DESC:
					return e2.getId() > e1.getId() ? 1 : (e1.getId().equals(e2.getId()) ? 0 : -1);
				default:
					return e1.getId() > e2.getId() ? 1 : (e1.getId().equals(e2.getId()) ? 0 : -1);
				}

			}
		};
		Collections.sort(tasks, comp);
	}

	/**
	 * Attempt to delete the task, only if it has no children or unpaid time
	 */
	public static void delete(final Context context, final Task task, final OnDeleteListener listener) {
		if (task.getDate() != null) {
			Alert.show(context, context.getResources().getString(R.string.errorCannotDelete));
		} else {
			new AlertDialog.Builder(context).setTitle(context.getResources().getString(R.string.confirmDeletion))
					.setMessage(context.getResources().getString(R.string.confirmDeletionText))
					.setIcon(R.drawable.ic_dialog_alert)
					.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							task.setDeleted(true);
							task.save();
                            if(task.getParent() != null) {
                                task.getParent().clearList();
                            }
                            for(Task child : task.getTasks()) {
                                child.setDeleted(true);
                                child.save();
                            }
                            task.invalidate();

                            Tracker.trackTask(Tracker.ACTION_DELETE, "TaskActions");
                            Alert.show(context, context.getResources().getString(R.string.taskDeleteSuccess));
							GlobalEntityChangeManager.notifyListeners(Task.class,
									GlobalEntityChangeManager.ACTION_DELETE);
							if(listener != null) {
								listener.delete();
							}
						}
					}).setNegativeButton(android.R.string.no, null).show();
		}
	}

	/**
	 * Views time logged for a given task
	 */
	public static void viewLog(Context context, Task task) {
		Intent intent = new Intent(context, ViewLogActivity.class);
		intent.putExtra(ViewLogActivity.DATA_TASK_ID, task.getId());
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}

	/**
	 * Shows the log options
	 */
	public static void showLogActions(Context context, TaskLog log) {
        Tracker.trackTask(Tracker.ACTION_SHOW_LOG_ACTIONS, "TaskActions");
		Intent intent = new Intent(context, LogOptionsActivity.class);
		intent.putExtra(LogOptionsActivity.DATA_SUBTASK_ID, log.getId());
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}
	
	public interface OnDeleteListener {
		public void delete();
	}
}