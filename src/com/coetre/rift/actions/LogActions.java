package com.coetre.rift.actions;

import java.util.Calendar;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.InputType;
import android.widget.EditText;

import com.activeandroid.query.Delete;
import com.coetre.rift.EditLogActivity;
import com.coetre.rift.LogAssignActivity;
import com.coetre.rift.R;
import com.coetre.rift.entities.EntityChangeManager;
import com.coetre.rift.entities.GlobalEntityChangeManager;
import com.coetre.rift.entities.Task;
import com.coetre.rift.entities.TaskLog;
import com.coetre.rift.utils.Alert;
import com.coetre.rift.utils.Config;
import com.coetre.rift.utils.Tracker;
import com.google.analytics.tracking.android.EasyTracker;

public class LogActions {
	/**
	 * Starts the delet Log process
	 * @param context
	 * @param log
	 * @param callback
	 */
	public static void delete(final Context context, final TaskLog log, final LogCallback callback) {
		new AlertDialog.Builder(context).setTitle(context.getResources().getString(R.string.confirmDeletion))
		.setMessage(context.getResources().getString(R.string.confirmLogDeletionText))
		.setIcon(R.drawable.ic_dialog_alert)
		.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				Task task = log.getTask();
				int seconds = log.getSeconds();
				Alert.show(context, "Removing " + seconds + " seconds");
				new Delete().from(TaskLog.class).where("Id = ?", log.getId()).execute();
				task.incrementSeconds(-seconds);
				task.save();

                Tracker.trackLog(Tracker.ACTION_DELETE, "LogActions");
				EntityChangeManager.notifyListeners(task);
				GlobalEntityChangeManager.notifyListeners(TaskLog.class, GlobalEntityChangeManager.ACTION_DELETE);
                GlobalEntityChangeManager.notifyListeners(Task.class, GlobalEntityChangeManager.ACTION_CHANGE);
				if(callback != null) {
					callback.run();
				}
			}
		}).setNegativeButton(android.R.string.no, null).show();
	}
	
	/**
	 * Starts the Edit Log dialog
	 * @param context
	 * @param log
	 * @param callback
	 */
	public static void edit(final Context context, final TaskLog log, final LogCallback callback) {
        Tracker.trackLog(Tracker.ACTION_EDIT, "LogActions");
        Intent intent = new Intent(context, EditLogActivity.class);
        intent.putExtra(EditLogActivity.DATA_LOG_ID, log.getId());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
	}
	
	/**
	 * Starts the Add Log dialog
	 * @param context
	 * @param task
	 * @param callback
	 */
	public static void add(final Context context, final Task task, final LogCallback callback) {
		final EditText editText = new EditText(context);
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
		new AlertDialog.Builder(context).setTitle(context.getResources().getString(R.string.addLogTitle))
		.setMessage(context.getResources().getString(R.string.addLogText))
		.setIcon(R.drawable.ic_dialog_info)
		.setView(editText)
		.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String secondsText = editText.getText().toString();
				try {
					int seconds = Integer.parseInt(secondsText);
					if(seconds < 1) {
						Alert.show(context,context.getResources().getString(R.string.editLogError));
					} else {
						Alert.show(context,context.getResources().getString(R.string.addLogSuccess));

                        Tracker.trackLog(Tracker.ACTION_CREATE, "LogActions");
						new TaskLog(task, Calendar.getInstance().getTime(), Calendar.getInstance().getTime(), seconds).save();
						task.incrementSeconds(seconds);
						task.save();
						EntityChangeManager.notifyListeners(task);
						GlobalEntityChangeManager.notifyListeners(TaskLog.class, GlobalEntityChangeManager.ACTION_NEW);
						if(callback != null) {
							callback.run();
						}
					}
				} catch(NumberFormatException exception) {
					Alert.show(context,context.getResources().getString(R.string.editLogError));
				}
			}
		}).setNegativeButton(android.R.string.no, null).show();
	}
	
	/**
	 * Starts the Reassign Time dialog
	 * @param context
	 * @param log
	 * @param callback
	 */
	public static void moveTime(final Context context, final TaskLog log, final LogCallback callback) {
        Tracker.trackLog(Tracker.ACTION_MOVE_TIME, "LogActions");
		Intent intent = new Intent(context, LogAssignActivity.class);
		intent.putExtra(LogAssignActivity.DATA_LOG_ID, log.getId());
		context.startActivity(intent);
	}
	
	public interface LogCallback {
		public void run();
	}
}