package com.coetre.rift;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Spinner;
import com.activeandroid.query.Select;
import com.coetre.rift.adapters.ColorAdapter;
import com.coetre.rift.entities.*;
import com.coetre.rift.utils.Alert;
import com.google.analytics.tracking.android.EasyTracker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EditClientActivity extends Activity implements OnClickListener {
    public final static String DATA_CLIENT_ID = "clientId";
    Spinner colorList;
    Client client = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_client);

        findViewById(R.id.cancelButton).setOnClickListener(this);
        findViewById(R.id.addButton).setOnClickListener(this);

        if (getIntent().getExtras().containsKey(DATA_CLIENT_ID)) {
            client = new Select().from(Client.class).where("Id = ?", getIntent()
                    .getLongExtra(DATA_CLIENT_ID, 0)).executeSingle();
            EntityManager<Client> entityManager = EntityManager.getInstance(Client.class);
            client = entityManager.get(client);
        }

        colorList = (Spinner) findViewById(R.id.colorList);
        ArrayList<String> colors = new ArrayList<String>();
        Collections.addAll(colors, getResources().getStringArray(R.array.colors));
        ColorAdapter adapter = new ColorAdapter(getBaseContext(), colors);
        colorList.setAdapter(adapter);

        if(client != null) {
            for(int i=0;i<colors.size();i++) {
                if(colors.get(i).toUpperCase().equals(client.getColor().toUpperCase())) {
                    colorList.setSelection(i);
                    break;
                }
            }
            ((EditText) findViewById(R.id.clientName)).setText(client.getName());
            ((EditText) findViewById(R.id.rate)).setText(Integer.toString(client.getRate()));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancelButton:
                finish();
                break;
            case R.id.addButton:
                String name = ((EditText) findViewById(R.id.clientName)).getText().toString();
                String rate = ((EditText) findViewById(R.id.rate)).getText().toString();
                String color = (String) colorList.getSelectedItem();
                int actualRate = 0;

                boolean validRate = true;
                try {
                    actualRate = Integer.parseInt(rate);
                } catch (NumberFormatException e) {
                    validRate = false;
                }

                // Form validation
                if (name.equals("")) {
                    Alert.show(getApplicationContext(), getResources().getString(R.string.clientNameEmpty));
                } else if(!validRate) {
                    Alert.show(getApplicationContext(), getResources().getString(R.string.clientRateEmpty));
                } else {
                    client.setName(name);
                    client.setColor(color);
                    client.setRate(actualRate);
                    client.save();

                    Alert.show(getApplicationContext(), getResources().getString(R.string.clientSaveSuccess));
                    List<Task> tasks = client.getTasks();
                    EntityChangeManager.notifyListeners(tasks);

                    GlobalEntityChangeManager.notifyListeners(Client.class, GlobalEntityChangeManager.ACTION_CHANGE);
                    EntityChangeManager.notifyListeners(client);
                    setResult(RESULT_OK, getIntent());
                    finish();
                }
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance().activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance().activityStop(this);
    }
}