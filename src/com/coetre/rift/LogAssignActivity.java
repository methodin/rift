package com.coetre.rift;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.activeandroid.query.Select;
import com.coetre.rift.adapters.TaskAdapter;
import com.coetre.rift.entities.*;
import com.coetre.rift.utils.Alert;
import com.google.analytics.tracking.android.EasyTracker;

public class LogAssignActivity extends Activity implements OnClickListener, OnItemClickListener {
	public static final String DATA_LOG_ID = "logId";

	private TaskLog log;
	private ArrayList<Task> tasks = new ArrayList<Task>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_log_assign);

        ListView taskList = (ListView) findViewById(R.id.taskList);

		if(getIntent().getExtras() != null && getIntent().getExtras().containsKey(DATA_LOG_ID)) {
			log = new Select().from(TaskLog.class).where("Id = ?", getIntent().getLongExtra(DATA_LOG_ID, 0)).executeSingle();
		} else {
			finish();
		}
		
		EntityManager<TaskLog> entityManager = EntityManager.getInstance(TaskLog.class);
		log = entityManager.get(log);
        List<Task> childTasks;
		final List<Task> parentTasks = new Select().from(Task.class).where("Deleted!=1 AND Parent IS NULL AND Id!=?",log.getTask().getId()).orderBy("Name ASC").execute();
        for(Task task : parentTasks) {
            tasks.add(task);
            childTasks = new Select().from(Task.class).where("Deleted!=1 AND Parent=? AND Id!=?", task.getId(),log.getTask().getId()).orderBy("Name ASC").execute();
            for(Task childTask : childTasks) {
                tasks.add(childTask);
            }
        }

        findViewById(R.id.cancelButton).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

		EntityManager<Task> taskEntityManager = EntityManager.getInstance(Task.class);
		tasks = taskEntityManager.filter(tasks);

		TaskAdapter adapter = new TaskAdapter(getBaseContext(), R.layout.view_task_select, tasks, log.getTask());
		taskList.setAdapter(adapter);
		taskList.setOnItemClickListener(this);
	}

	@Override
	public void onClick(View v) {
		finish();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		int seconds = log.getSeconds();
		
		Task oldTask = log.getTask();
		oldTask.incrementSeconds(-seconds);
		oldTask.save();
		
		Task task = tasks.get(position);
		task.incrementSeconds(seconds);
		task.save();
		
		log.setTask(task);
		log.save();

        Alert.show(getApplicationContext(), getResources().getString(R.string.logSaveSuccess));
		EntityChangeManager.notifyListeners(task);
		EntityChangeManager.notifyListeners(oldTask);
		GlobalEntityChangeManager.notifyListeners(TaskLog.class, GlobalEntityChangeManager.ACTION_CHANGE);
		finish();
	}

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance().activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance().activityStop(this);
    }
}