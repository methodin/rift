package com.coetre.rift;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.activeandroid.query.Select;
import com.coetre.rift.entities.Invoice;
import com.coetre.rift.entities.InvoiceClient;
import com.coetre.rift.loaders.InvoiceLoader;
import com.coetre.rift.views.InvoiceClientView;
import com.google.analytics.tracking.android.EasyTracker;

public class ViewInvoiceActivity extends Activity {
    public final static String DATA_INVOICE_ID = "invoiceId";
    private LinearLayout invoiceRecords;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_invoice);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        final TextView dateView = (TextView) findViewById(R.id.date);
        final TextView invoiceView = (TextView) findViewById(R.id.invoice);
        final TextView amountView = (TextView) findViewById(R.id.amount);
        final TextView timeView = (TextView) findViewById(R.id.time);
        invoiceRecords = (LinearLayout) findViewById(R.id.invoiceRecords);

        if(getIntent().getExtras() != null&& getIntent().getExtras().containsKey(DATA_INVOICE_ID)) {
            new InvoiceLoader(new InvoiceLoader.OnInvoiceLoaderCompleted() {
                @Override
                public void onInvoiceLoaderCompleted(Invoice invoice) {
                    dateView.setText(invoice.getFormattedDate());
                    invoiceView.setText(Long.toString(invoice.getId()));
                    amountView.setText(invoice.getFormattedTotal());
                    timeView.setText(invoice.getFormattedTime());

                    for(InvoiceClient invoiceClient : invoice.getInvoiceClients()) {
                        invoiceRecords.addView(new InvoiceClientView(getBaseContext(), invoiceClient));
                    }

                    setTitle(invoice.getFormattedDate()+" - "+invoice.getFormattedTotal());

                }
            }).execute(new Select().from(Invoice.class).where("Id = ?", getIntent().getExtras().getLong(DATA_INVOICE_ID)));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance().activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance().activityStop(this);
    }
}