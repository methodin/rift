package com.coetre.rift.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.widget.Toast;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.coetre.rift.R;
import com.coetre.rift.entities.*;
import com.coetre.rift.utils.*;
import com.google.analytics.tracking.android.EasyTracker;

import java.io.IOException;

public class SettingsFragment extends PreferenceFragment implements Preference.OnPreferenceClickListener, SharedPreferences.OnSharedPreferenceChangeListener {
    private final static String KEY_BACKUP = "backup";
    private final static String KEY_RESTORE = "restore";
    private final static String KEY_DELETE = "delete";
    private final static String KEY_HELP = "help";
    private final static String KEY_VERSION = "version";
    private final static String KEY_BACKUP_PATH = "backupPath";

    public static final String KEY_PREF_TIMER = "pref_timer";

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(KEY_PREF_TIMER)) {
            Intent intent = new Intent();
            intent.setAction("com.coetre.rift.intent.action.START_TIMER");
            SettingsFragment.this.getActivity().sendBroadcast(intent);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
        EasyTracker.getInstance().setContext(getActivity().getApplicationContext());
        EasyTracker.getTracker().sendView("/SettingsFragment");

        Preference button = findPreference(KEY_BACKUP);
        if(button != null) {
            button.setOnPreferenceClickListener(this);
        }
        button = findPreference(KEY_RESTORE);
        if(button != null) {
            button.setOnPreferenceClickListener(this);
        }
        button = findPreference(KEY_DELETE);
        if(button != null) {
            button.setOnPreferenceClickListener(this);
        }
        button = findPreference("populate");
        if(button != null) {
            button.setOnPreferenceClickListener(this);
        }
        button = findPreference(KEY_HELP);
        if(button != null) {
            button.setOnPreferenceClickListener(this);
        }

        button = findPreference(KEY_VERSION);
        if(button != null) {
            String versionName = "Unknown";
            try {
                versionName = getActivity().getPackageManager()
            .getPackageInfo(getActivity().getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
            }

            button.setSummary(versionName);
        }

        button = findPreference(KEY_BACKUP_PATH);
        if(button != null) {
            try {
                button.setSummary(JsonDataManager.getFileDirectory() + JsonDataManager.FILE_NAME);
            } catch(IOException e) {
                button.setSummary("Unknown");
            }
        }
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        if(preference.getKey().equals(KEY_BACKUP)) {
            Tracker.trackSettings(Tracker.ACTION_CLICK, "backup");
            boolean result = new JsonDataManager().backup();
            String message = result ? getResources().getString(R.string.backupSuccess) : getResources().getString(R.string.backupFail);
            Toast.makeText(SettingsFragment.this.getActivity(), message, Toast.LENGTH_LONG).show();
        } else if(preference.getKey().equals(KEY_RESTORE)) {
            Tracker.trackSettings(Tracker.ACTION_CLICK, "restore");
            int count = new JsonDataManager().restore();
            String message = count > 0 ? getResources().getString(R.string.restoreSuccess)+" - "+count+" rows inserted" :
                    getResources().getString(R.string.restoreFail);
            Toast.makeText(SettingsFragment.this.getActivity(), message, Toast.LENGTH_LONG).show();
            GlobalEntityChangeManager.notifyListeners(Task.class, GlobalEntityChangeManager.ACTION_NEW);
            GlobalEntityChangeManager.notifyListeners(Client.class, GlobalEntityChangeManager.ACTION_NEW);
            GlobalEntityChangeManager.notifyListeners(Invoice.class, GlobalEntityChangeManager.ACTION_NEW);
        } else if(preference.getKey().equals(KEY_HELP)) {
            String url = "http://www.coetre.com/products/rift/support";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        } else if(preference.getKey().equals(KEY_DELETE)) {
            new Delete().from(TaskLog.class).execute();
            new Delete().from(InvoiceRecord.class).execute();
            new Delete().from(InvoiceClient.class).execute();
            new Delete().from(Task.class).execute();
            new Delete().from(Client.class).execute();
            new Delete().from(Invoice.class).execute();
            Toast.makeText(SettingsFragment.this.getActivity(), "Delete complete", Toast.LENGTH_SHORT).show();
            GlobalEntityChangeManager.notifyListeners(Task.class, GlobalEntityChangeManager.ACTION_NEW);
            GlobalEntityChangeManager.notifyListeners(Client.class, GlobalEntityChangeManager.ACTION_NEW);
            GlobalEntityChangeManager.notifyListeners(Invoice.class, GlobalEntityChangeManager.ACTION_NEW);
        } else if(preference.getKey().equals("populate")) {
            new Delete().from(TaskLog.class).execute();
            new Delete().from(InvoiceRecord.class).execute();
            new Delete().from(InvoiceClient.class).execute();
            new Delete().from(Task.class).execute();
            new Delete().from(Client.class).execute();
            new Delete().from(Invoice.class).execute();

			new Client("Microsoft","#e67e22", 0).save();
			new Client("Google","#2980b9",105).save();
			new Client("Amazon","#9b59b6",220).save();
			Client microsoft = new Select().from(Client.class)
					.where("Name = ?", "Microsoft").executeSingle();
			Client google = new Select().from(Client.class)
					.where("Name = ?", "Google").executeSingle();
			Client amazon = new Select().from(Client.class)
					.where("Name = ?", "Amazon").executeSingle();

			new Task("Mobile App", 1234, null, amazon).save();
			new Task("Site Redesign", 5555, null, microsoft).save();
			new Task("Billing Component", 4444, null, google).save();
			new Task("API Base",7234, null, google).save();
			new Task("Outlook Integration", 1245, null, amazon).save();
			new Task("Better Phone", 8456, null, microsoft).save();
			new Task("Cloud Upgrade", 7253, null, null).save();
			new Task("Add memory to laptop", 1512, null,null).save();
			new Task("Copy Google", 9586, null,microsoft).save();

			Task task = new Select().from(Task.class)
					.where("Name = ?", "Site Redesign").executeSingle();
			new Task("Come up with color scheme", 3333, task, null).save();
			new Task("Test mobile responsiveness", 2222, task, null).save();
			new Task("Implement angular", 0, task, null).save();

			task = new Select().from(Task.class)
					.where("Name = ?", "Billing Component")
					.executeSingle();
			new Task("Develop API standards", 2222, task, null).save();
			new Task("Determine language - dart, go?", 2222, task, null).save();
			GlobalEntityChangeManager.notifyListeners(Task.class, GlobalEntityChangeManager.ACTION_NEW);
        }
        return true;
    }
}