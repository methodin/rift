package com.coetre.rift.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import com.activeandroid.query.Select;
import com.coetre.rift.R;
import com.coetre.rift.actions.TaskActions;
import com.coetre.rift.adapters.TaskCardAdapter;
import com.coetre.rift.entities.GlobalEntityChangeManager;
import com.coetre.rift.entities.GlobalEntityChangeManager.GlobalEntityChangeListener;
import com.coetre.rift.entities.Task;
import com.coetre.rift.loaders.TaskListLoader;
import com.coetre.rift.utils.Config;
import com.coetre.rift.utils.TaskActiveChangeManager;
import com.coetre.rift.utils.Tracker;
import com.google.analytics.tracking.android.EasyTracker;

/**
 * A dummy fragment representing a section of the app, but that simply displays
 * dummy text.
 */
public class TaskCardFragment extends Fragment implements FragmentInterface, GlobalEntityChangeListener {
    public final static String DATA_TYPE = "type";
    private GridView gridView;
    private int type = Config.MODE_TASK;
    private String query = "Parent IS NULL AND Deleted != 1";
    private View emptyView;

    public TaskCardFragment() {
        GlobalEntityChangeManager.add(Task.class, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle bundle) {
        if(getArguments().containsKey(DATA_TYPE)) {
            type = getArguments().getInt(DATA_TYPE);
        }

        EasyTracker.getInstance().setContext(getActivity().getApplicationContext());
        EasyTracker.getTracker().sendView("/TaskCardFragment");
        setHasOptionsMenu(true);

        View rootView = inflater.inflate(R.layout.fragment_task, container,
                false);

        emptyView = rootView.findViewById(android.R.id.empty);
        gridView = (GridView) rootView.findViewById(R.id.cardList);

        if(type == Config.MODE_ACTIVE) {
            query = "Deleted != 1 AND Seconds > 0";
            GlobalEntityChangeManager.add(Task.class, new GlobalEntityChangeListener() {
                @Override
                public void change(int action) {
                    TaskCardFragment.this.change(GlobalEntityChangeManager.ACTION_CHANGE);
                }
            });
            TaskActiveChangeManager.add(new TaskActiveChangeManager.TaskActiveChangeListener() {
                @Override
                public void change() {
                    TaskCardFragment.this.change(GlobalEntityChangeManager.ACTION_CHANGE);
                }
            });
        }

        TaskCardAdapter adapter = new TaskCardAdapter(rootView.getContext(), R.layout.view_task_card, null);
        gridView.setAdapter(adapter);

        new TaskListLoader(gridView, emptyView).execute(new Select().from(Task.class).where(query));

        return rootView;
    }

    @Override
    public void hideMenuActions() {
    }

    @Override
    public void showMenuActions() {
    }

    @Override
    public void dispatchSort(int sort) {
        if (Config.sort != 0) {
            Tracker.trackTask(Tracker.ACTION_SORT, "TaskCardFragment", (long)type);
            TaskActions.sort(Config.sort, ((TaskCardAdapter) gridView.getAdapter()).getTasks());
            ((TaskCardAdapter) gridView.getAdapter()).notifyDataSetChanged();
        }
    }

    @Override
    public void change(int action) {
        new TaskListLoader(gridView, emptyView).execute(new Select().from(Task.class).where(query));
    }
}