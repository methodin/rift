package com.coetre.rift.fragments;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.*;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import com.activeandroid.query.Select;
import com.coetre.rift.AddClientActivity;
import com.coetre.rift.R;
import com.coetre.rift.ViewClientActivity;
import com.coetre.rift.actions.ClientActions;
import com.coetre.rift.adapters.ClientAdapter;
import com.coetre.rift.entities.Client;
import com.coetre.rift.entities.EntityManager;
import com.coetre.rift.entities.GlobalEntityChangeManager;
import com.coetre.rift.entities.GlobalEntityChangeManager.GlobalEntityChangeListener;
import com.coetre.rift.utils.Alert;
import com.coetre.rift.utils.Config;
import com.google.analytics.tracking.android.EasyTracker;

/**
 * A list of clients
 */
public class ClientListFragment extends Fragment implements FragmentInterface,AdapterView.OnItemClickListener {
    private ClientAdapter adapter;
    private boolean shouldShowMenu = true;
    private List<Client> selectedClients;
    private GridView gridView;

	public ClientListFragment() {
	}

	@Override
	public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.fragment_client_list, container, false);

        EasyTracker.getInstance().setContext(getActivity().getApplicationContext());
        EasyTracker.getTracker().sendView("/ClientListFragment");
        gridView = (GridView) rootView.findViewById(android.R.id.list);
        gridView.setOnItemClickListener(this);

		GlobalEntityChangeManager.add(Client.class, new GlobalEntityChangeListener() {
			@Override
			public void change(int action) {
				switch (action) {
				case GlobalEntityChangeManager.ACTION_DELETE:
				case GlobalEntityChangeManager.ACTION_NEW:
					updateItems(rootView);
					break;
				case GlobalEntityChangeManager.ACTION_CHANGE:
                    updateItems(rootView);
					break;
				}
			}
		});
		
		updateItems(rootView);

		return rootView;
	}

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        gridView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        gridView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position,
                                                  long id, boolean checked) {
                gridView.setSelection(position);
                Client client = ((ClientAdapter) gridView.getAdapter()).getItem(position);
                if (client.getRate() > 0 && client.getUnpaidSeconds() > 0) {
                    if (checked) {
                        selectedClients.add(client);
                    } else {
                        selectedClients.remove(client);
                    }
                    mode.setTitle(Integer.toString(selectedClients.size()) + " selected");
                } else if (checked) {
                    gridView.setItemChecked(position, false);
                }
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                // Respond to clicks on the actions in the CAB
                switch (item.getItemId()) {
                    case R.id.actionInvoice:
                        ClientActions.invoice(ClientListFragment.this.getActivity(), selectedClients);
                        mode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.client_context, menu);
                selectedClients = new ArrayList<Client>();
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Intent intent = new Intent(getActivity(), ViewClientActivity.class);
        intent.putExtra(ViewClientActivity.DATA_CLIENT_ID, adapter.getItem(position).getId());
        startActivity(intent);
    }

    @Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
		inflater.inflate(R.menu.client, menu);
	}

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.actionClientAdd).setVisible(shouldShowMenu);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.actionClientAdd:
			Intent intent = new Intent(getActivity(), AddClientActivity.class);
			startActivity(intent);
			break;
        case R.id.actionInvoice:
            if(gridView.getCount() > 0) {
                for(int i=0;i<gridView.getCount();i++) {
                    gridView.setItemChecked(i, true);
                }
            } else {
                Alert.show(getActivity(), getResources().getString(R.string.errorBillableClients));
            }
            break;
		}
        super.onOptionsItemSelected(item);
		return false;
	}

    @Override
    public void hideMenuActions() {
        shouldShowMenu = false;
    }

    @Override
    public void showMenuActions() {
        shouldShowMenu = true;
    }

    @Override
    public void dispatchSort(int sort) {

    }

    /**
	 * Update all views
	 * 
	 * @param rootView
	 */
	protected void updateItems(View rootView) {
        gridView.setAdapter(null);
		List<Client> clients = new Select().from(Client.class).where("Deleted != 1").orderBy("Name ASC").execute();
		
		setHasOptionsMenu(true);

		// Set empty view if necessary
		rootView.findViewById(android.R.id.empty).setVisibility(clients.size() == 0 ? View.VISIBLE : View.GONE);
		rootView.findViewById(android.R.id.list).setVisibility(clients.size() == 0 ? View.GONE : View.VISIBLE);

		EntityManager<Client> manager = EntityManager.getInstance(Client.class);
		clients = manager.filter(clients);
		adapter = new ClientAdapter(rootView.getContext(), clients, false, ClientAdapter.MODE_LIST);
		gridView.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}
}