package com.coetre.rift.fragments;

public interface FragmentInterface {
    public void hideMenuActions();
    public void showMenuActions();
    public void dispatchSort(int sort);
}