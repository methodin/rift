package com.coetre.rift.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.*;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import com.activeandroid.query.Select;
import com.coetre.rift.R;
import com.coetre.rift.ViewClientActivity;
import com.coetre.rift.ViewInvoiceActivity;
import com.coetre.rift.actions.ClientActions;
import com.coetre.rift.adapters.InvoiceAdapter;
import com.coetre.rift.entities.Client;
import com.coetre.rift.entities.GlobalEntityChangeManager;
import com.coetre.rift.entities.GlobalEntityChangeManager.GlobalEntityChangeListener;
import com.coetre.rift.entities.Invoice;
import com.coetre.rift.utils.Alert;
import com.coetre.rift.utils.Config;
import com.coetre.rift.utils.Tracker;
import com.google.analytics.tracking.android.EasyTracker;

import java.util.ArrayList;
import java.util.List;

/**
 * A list of clients
 */
public class InvoiceListFragment extends Fragment implements FragmentInterface, AdapterView.OnItemClickListener {
    private InvoiceAdapter adapter;
    private GridView gridView;

    public InvoiceListFragment() {
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_invoice_list, container, false);
        gridView = (GridView) rootView.findViewById(android.R.id.list);
        gridView.setOnItemClickListener(this);

        EasyTracker.getInstance().setContext(getActivity().getApplicationContext());
        EasyTracker.getTracker().sendView("/InvoiceListFragment");
        GlobalEntityChangeManager.add(Invoice.class, new GlobalEntityChangeListener() {
            @Override
            public void change(int action) {
                updateItems(rootView);
            }
        });

        updateItems(rootView);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.invoice, menu);
    }

    @Override
    public void hideMenuActions() {
    }

    @Override
    public void showMenuActions() {
    }

    @Override
    public void dispatchSort(int sort) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.actionInvoice:
                Tracker.trackInvoice(Tracker.ACTION_CREATE, "InvoiceListFragment");
                showClientSelectDialog();
                break;
        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Tracker.trackInvoice(Tracker.ACTION_VIEW, "InvoiceListFragment");
        Intent intent = new Intent(getActivity(), ViewInvoiceActivity.class);
        intent.putExtra(ViewInvoiceActivity.DATA_INVOICE_ID, adapter.getItem(position).getId());
        startActivity(intent);
    }

    /**
     * Update all views
     *
     * @param rootView
     */
    protected void updateItems(View rootView) {
        gridView.setAdapter(null);
        List<Invoice> invoices = new Select().from(Invoice.class).orderBy("Id DESC").execute();

        setHasOptionsMenu(true);

        // Set empty view if necessary
        rootView.findViewById(android.R.id.empty).setVisibility(invoices.size() == 0 ? View.VISIBLE : View.GONE);
        rootView.findViewById(android.R.id.list).setVisibility(invoices.size() == 0 ? View.GONE : View.VISIBLE);

        adapter = new InvoiceAdapter(rootView.getContext(), R.layout.row_invoice, invoices);
        gridView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    protected void showClientSelectDialog() {
        final List<Client> clients = new Select().from(Client.class).where("Deleted != 1 AND Rate > 0").orderBy("Name ASC").execute();
        final ArrayList<Client> validClients = new ArrayList<Client>();

        final ArrayList<Client> selectedClients = new ArrayList<Client>();  // Where we track the selected items
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Set the dialog title
        ArrayList<String> options = new ArrayList<String>();
        for(Client client : clients) {
            if(client.getUnpaidSeconds() > 0) {
                validClients.add(client);
                String clientName = client.getName() + " ("+client.getFormattedRate(client.getUnpaidSeconds())+")";
                options.add(clientName);
            }
        }

        if(options.size() > 0) {
            builder.setTitle(R.string.selectClientsToInvoice)
                    // Specify the list array, the items to be selected by default (null for none),
                    // and the listener through which to receive callbacks when items are selected
                    .setMultiChoiceItems(options.toArray(new String[options.size()]), null,
                            new DialogInterface.OnMultiChoiceClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which,
                                                    boolean isChecked) {
                                    Client client = validClients.get(which);
                                    if (isChecked) {
                                        // If the user checked the item, add it to the selected items
                                        selectedClients.add(client);
                                    } else if (selectedClients.contains(client)) {
                                        // Else, if the item is already in the array, remove it
                                        selectedClients.remove(client);
                                    }
                                }
                            })
                            // Set the action buttons
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                        ClientActions.invoice(InvoiceListFragment.this.getActivity(), selectedClients);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            builder.create().show();
        } else {
            Alert.show(InvoiceListFragment.this.getActivity(), R.string.noUnpaidTasks);
        }
    }
}