package com.coetre.rift.entities;

import java.text.DecimalFormat;
import java.util.List;

import android.graphics.Color;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "Client")
public class Client extends Model implements Notifiable {
	/**
	 * The name of the client
	 */
	@Column(name = "Name")
	protected String name;
	
	/**
     * The color of the client
	 */
	@Column(name = "Color")
	protected String color;

    /**
     * Deleted status
     */
    @Column(name = "Deleted")
    protected boolean deleted;

    /**
     * Hourly rate
     */
    @Column(name = "Rate")
    protected int rate;

    private static DecimalFormat formatter = new DecimalFormat("#,###.00");

    public Client(String name, String color, int rate) {
		super();
		this.name = name;
		this.color = color;
        this.rate = rate;
	}
	public Client() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}
	
	public int getColorAsInt() {
		return Color.parseColor(color);
	}

	public void setColor(String color) {
		this.color = color;
	}

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public List<Task> getTasks() {
		List<Task> list = getMany(Task.class, "Client");
		EntityManager<Task> taskManager = EntityManager.getInstance(Task.class);
		list = taskManager.filter(list);
		for(int i=0;i<list.size();i++) {
			if(list.get(i).getDeleted()) {
				list.remove(i);
				i--;
			}
		}
		return list;
	}
	
	public int getTaskCount() {
		List<Task> list = getTasks();
		for(int i=0;i<list.size();i++) {
			if(list.get(i).getDeleted()) {
				list.remove(i);
				i--;
			}
		}
		return list.size();
	}

    public int getUnpaidSeconds() {
        int seconds = 0;
        List<Task> list = getTasks();
        for(int i=0;i<list.size();i++) {
            if(list.get(i).getSeconds() > 0) {
                seconds += list.get(i).getSeconds();
            }
        }
        return seconds;
    }

    public double getAppliedRate(int seconds) {
        if(rate > 0) {
            double amount = ((double)seconds/3600)*(double)rate;
            String rounded = String.format("%.2f", amount);
            return Double.parseDouble(rounded);
        }
        return 0;
    }
    public String getFormattedRate(int seconds) {
        if(rate > 0 && seconds > 0) {
            return "$"+formatter.format(getAppliedRate(seconds));
        }
        return "";
    }

    @Override
    public void invalidate() {

    }

    @Override
    public String getKey() {
        return "Client"+Long.toString(getId());
    }


}