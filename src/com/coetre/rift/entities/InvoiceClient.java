package com.coetre.rift.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.text.DecimalFormat;
import java.util.List;

@Table(name = "InvoiceClient")
public class InvoiceClient extends Model {
    /**
     * The invoice reference
     */
    @Column(name = "Invoice")
    protected Invoice invoice;

    /**
     * Total number of seconds on this invoice
     */
    @Column(name = "Seconds")
    private int seconds;

    /**
     * The client being billed
     */
    @Column(name = "Client")
    private Client client;

    /**
     * The rate to bill at
     */
    @Column(name = "Rate")
    private int rate;

    /**
     * The total amount billed
     */
    @Column(name = "Total")
    private double total;

    private static DecimalFormat formatter = new DecimalFormat("#,###.00");

    public InvoiceClient() {
        super();
    }

    public InvoiceClient(Invoice invoice, int seconds, Client client, int rate, double total) {
        this.invoice = invoice;
        this.seconds = seconds;
        this.client = client;
        this.rate = rate;
        this.total = total;
    }

    public double getTotal() {
        return total;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public int getSeconds() {
        return seconds;
    }

    public Client getClient() {
        return client;
    }

    public int getRate() {
        return rate;
    }

    public List<InvoiceRecord> getInvoiceRecords() {
        return getMany(InvoiceRecord.class, "InvoiceClient");
    }

    public String getFormattedTotal() {
        return "$" + formatter.format(this.total);
    }
}