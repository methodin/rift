package com.coetre.rift.entities;

import android.os.Parcel;
import android.os.Parcelable;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Table(name = "Task")
public class Task extends Model implements Parcelable, Notifiable {
	/**
	 * Object references
	 */
	private List<Task> list;

	/**
	 * The number of seconds tracked for the task (in the current period)
	 */
	@Column(name = "Seconds")
	protected int seconds;

	/**
	 * The name of the task
	 */
	@Column(name = "Name")
	protected String name = "";

	/**
	 * The parent task
	 */
	@Column(name = "Parent")
	protected Task parent;
	
	/**
	 * The most recent start date for an interval
	 */
	@Column(name = "Date")
	protected Date date; 
	
	/**
	 * The client the task belongs to
	 */
	@Column(name = "Client")
	protected Client client; 
	
	/**
	 * Deleted status
	 */
	@Column(name = "Deleted")
	protected boolean deleted;

    private StringBuilder builder = new StringBuilder();
    private DecimalFormat formatter = new DecimalFormat("#,###.00");

	public Task() {
		super();
	}

	public Task(String name, int seconds, Task parent, Client client) {
		super();
		this.name = name;
		this.seconds = seconds;
		this.parent = parent;
		this.client = client;
	}

	public Task(Parcel parcel) {
		readFromParcel(parcel);
	}

	/**
	 * Getters and setters
	 */
	public Task getParent() {
		if(parent == null) {
			return null;
		}
		EntityManager<Task> em = EntityManager.getInstance(Task.class);
		return em.add(parent);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public int getElapsedSeconds() {
		if(date == null) {
			return 0;
		}
		long diffInMs = Calendar.getInstance().getTime().getTime() - date.getTime();
		return (int) TimeUnit.MILLISECONDS.toSeconds(diffInMs);
	}

	public int getSeconds() {
		int childSeconds = getChildSeconds();
		return seconds + getElapsedSeconds() + childSeconds;
	}
    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }
    public int getChildSeconds() {
        int childSeconds = 0;
        if(list == null) {
            getTasks();
        }
        if (list.size() > 0) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                childSeconds += list.get(i).getSeconds();
            }
        }
        return childSeconds;
    }
	
	public void incrementSeconds(int seconds) {
		this.seconds += seconds;
	}

	public Client getClient() {
        if(client == null) {
            return null;
        }
        EntityManager<Client> em = EntityManager.getInstance(Client.class);
        return em.add(client);
	}

	public void setClient(Client client) {
		this.client = client;
	}
	
	public boolean getDeleted() {
		return this.deleted;
	}
	
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public List<Task> getTasks() {
        if(list == null) {
            if(getId() != null && getId() > 0) {
                list = getMany(Task.class, "Parent");
                for(int i=0,count=list.size();i<count;i++) {
                    if(list.get(i).getDeleted()) {
                        list.remove(i);
                        count--;
                        i--;
                    }
                }
                EntityManager<Task> taskManager = EntityManager.getInstance(Task.class);
                list = taskManager.filter(list);
            } else {
                list =  new ArrayList<Task>();
            }
        }
        return list;
	}

    public List<TaskLog> getTaskLogs() {
        return getMany(TaskLog.class, "Task");
    }

	/**
	 * Get seconds formatted as text
	 * 
	 * @return
	 */
    public String getFormattedTime() {
        return getFormattedTime(getSeconds());
    }
	public String getFormattedTime(int time) {
		int  h = 0, m = 0;
		int elapsedTime = time;
		if (elapsedTime >= 3600) {
			h = (int) Math.floor(elapsedTime / 3600);
			elapsedTime %= 3600;
		}
		if (elapsedTime >= 60) {
			m = (int) Math.floor(elapsedTime / 60);
			elapsedTime %= 60;
		}
		int s = elapsedTime;

        builder.setLength(0);
        builder.append(h > 9 ? "" : "0").append(h);
        builder.append(m > 9 ? ":" : ":0").append(m);
        builder.append(s > 9 ? ":" : ":0").append(s);

		return builder.toString();
	}

    public String getFormattedRate() {
        double rate = 0;
        if(client != null) {
            rate = getClient().getAppliedRate(getSeconds());
        } else if(parent != null&& parent.getClient() != null) {
            rate = getParent().getClient().getAppliedRate(getSeconds());
        }
        if(rate > 0) {
            return "$"+formatter.format(rate);
        }
        return "";
    }
	
	/**
	 * Is the task active?
	 * @return
	 */
	public boolean isActive() {
		return date != null;
	}
	
	/**
	 * Is the task active?
	 * @return
	 */
	public boolean isChildActive() {
		getTasks();
		if (list.size() > 0) {
			int size = list.size();
			for (int i = 0; i < size; i++) {
				if(list.get(i).isActive()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeInt(seconds);
		parcel.writeString(name);
		parcel.writeParcelable(parent, flags);
	}

	/**
	 * Reading from a Parcel
	 * 
	 * @param parcel
	 */
	public void readFromParcel(Parcel parcel) {
		seconds = parcel.readInt();
		name = parcel.readString();
		parent = parcel.readParcelable(null);
	}

	/**
	 * Invalidates a Task to notify any UI elements that something has changed
	 */
	public void invalidate() {
		if (parent != null) {
			EntityChangeManager.notifyListeners(getParent());
		}
	}
	
	/**
	 * Get the key for this task
	 */
	public String getKey() {
		return "Task" + getId();
	}

    public void clearList() {
        this.list = null;
    }
}