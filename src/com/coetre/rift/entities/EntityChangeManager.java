package com.coetre.rift.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.backup.BackupManager;
import com.activeandroid.Model;
import com.coetre.rift.utils.Config;
import com.coetre.rift.utils.Logger;

/**
 * Manager instance to handle Task objects
 */
public class EntityChangeManager {
	public static final int FLAG_UPDATE_CHILDREN = 1;

	/**
	 * List of Tasks
	 */
	private static HashMap<String, TaskContainer> tasks = new HashMap<String, TaskContainer>();

    private static String getKey(Object model) {
        return model.getClass().getSimpleName();
    }

	/**
	 * Add a Task to the list
	 * 
	 * @param cls
     * @param listener
	 */
	public static void add(Class<?> cls, EntityChangeListener listener) {
		String key = cls.getSimpleName();
		if (!tasks.containsKey(key)) {
            tasks.put(key, new TaskContainer());
		}
        try {
            tasks.get(key).add(listener);
        } catch(Exception e) {
        }
	}

	/**
	 * Destroy the change listener
	 *
     * @param cls
	 * @param listener
	 */
	public static void remove(Class<?> cls, EntityChangeListener listener) {
		String key = cls.getSimpleName();
		if (tasks.containsKey(key)) {
            try {
                tasks.get(key).remove(listener);
            } catch(Exception e) {
            }
		}
	}

	/**
	 * Notify all listeners that an item changed
	 *
	 * @param models
	 */
    public static void notifyListeners(List<? extends Model> models) {
        for(Model model : models) {
            notifyListeners((Notifiable)model);
        }
    }
	public static void notifyListeners(Notifiable model) {
        String key = getKey(model);
        if (tasks.containsKey(key)) {
            tasks.get(key).notifyListeners(model);
        }
        BackupManager.dataChanged(Config.PACKAGE_NAME);
	}
	public static void notifyListeners(Notifiable model, int flags) {
        String key = getKey(model);
        if (tasks.containsKey(key)) {
            tasks.get(key).notifyListeners(model);
        }

		if(model instanceof Task && (flags & FLAG_UPDATE_CHILDREN) == FLAG_UPDATE_CHILDREN) {
			List<Task> children = ((Task)model).getTasks();
			for(Task child : children) {
                key = getKey(child);
                if (tasks.containsKey(key)) {
                    tasks.get(key).notifyListeners(model);
                }
			}
		}
        BackupManager.dataChanged(Config.PACKAGE_NAME);
	}

	protected static class TaskContainer {
		public final ArrayList<EntityChangeListener> mListeners = new ArrayList<EntityChangeListener>();
		public void add(EntityChangeListener listener) {
            if(!mListeners.contains(listener)) {
			    mListeners.add(listener);
            }
		}
		public void remove(EntityChangeListener listener) {
			mListeners.remove(listener);
		}
		public void notifyListeners(Notifiable entity) {
            for(int i=0;i<mListeners.size();i++) {
                mListeners.get(i).change(entity);
            }
            entity.invalidate();
		}
	}

    public interface EntityChangeListener {
        /**
         * Entity has changed
         * @param <T>
         */
        public <T> void change(T task);
    }
}