package com.coetre.rift.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.coetre.rift.utils.Logger;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Table(name = "Invoice")
public class Invoice extends Model {
    /**
     * The date of the invoice
     */
    @Column(name = "Date")
    protected Date date;

    /**
     * Total number of seconds on this invoice
     */
    @Column(name = "Seconds")
    private int seconds;

    /**
     * Total amount billed on this invoice
     */
    @Column(name = "Total")
    private double total;

    private static DecimalFormat formatter = new DecimalFormat("#,###.00");
    private static SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
    private StringBuilder builder = new StringBuilder();

    public Invoice() {
        super();
    }

    public Invoice(Date date, int seconds, double total) {
        this.date = date;
        this.seconds = seconds;
        this.total = total;
    }

    public List<InvoiceClient> getInvoiceClients() {
        return getMany(InvoiceClient.class, "Invoice");
    }

    public Date getDate() {
        return date;
    }

    public int getSeconds() {
        return seconds;
    }

    public double getTotal() {
        return total;
    }

    public String getFormattedTotal() {
        return "$" + formatter.format(total);
    }

    public String getFormattedTime() {
        int h = 0, m = 0;
        int elapsedTime = seconds;
        if (elapsedTime >= 3600) {
            h = (int) Math.floor(elapsedTime / 3600);
            elapsedTime %= 3600;
        }
        if (elapsedTime >= 60) {
            m = (int) Math.floor(elapsedTime / 60);
            elapsedTime %= 60;
        }
        int s = elapsedTime;

        builder.setLength(0);
        builder.append(h > 9 ? "" : "0").append(h);
        builder.append(m > 9 ? ":" : ":0").append(m);
        builder.append(s > 9 ? ":" : ":0").append(s);

        return builder.toString();
    }

    public String getFormattedDate() {
        return dateFormatter.format(date);
    }
}