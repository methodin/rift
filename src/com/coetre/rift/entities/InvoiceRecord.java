package com.coetre.rift.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.text.DecimalFormat;

@Table(name = "InvoiceRecord")
public class InvoiceRecord extends Model {
    /**
     * The invoice reference
     */
    @Column(name = "InvoiceClient")
    protected InvoiceClient invoiceClient;

    /**
     * Total number of seconds on this invoice
     */
    @Column(name = "Seconds")
    private int seconds;

    /**
     * The task being billed
     */
    @Column(name = "Task")
    private Task task;

    /**
     * The total amount billed
     */
    @Column(name = "Total")
    private double total;

    /**
     * The total amount billed
     */
    @Column(name = "Parent")
    private Task parent;

    private static DecimalFormat formatter = new DecimalFormat("#,###.00");
    private StringBuilder builder = new StringBuilder();

    public InvoiceRecord() {
        super();
    }

    public InvoiceRecord(InvoiceClient invoiceClient, int seconds, Task task, double total, Task parent) {
        this.invoiceClient = invoiceClient;
        this.seconds = seconds;
        this.task = task;
        this.total = total;
        this.parent = parent;
    }

    public double getTotal() {
        return total;
    }

    public InvoiceClient getInvoiceClient() {
        return invoiceClient;
    }

    public int getSeconds() {
        return seconds;
    }

    public Task getTask() {
        return task;
    }

    public Task getParent() {
        return parent;
    }

    public String getFormattedTotal() {
        return "$" + formatter.format(total);
    }

    public String getFormattedTime() {
        int h = 0, m = 0;
        int elapsedTime = seconds;
        if (elapsedTime >= 3600) {
            h = (int) Math.floor(elapsedTime / 3600);
            elapsedTime %= 3600;
        }
        if (elapsedTime >= 60) {
            m = (int) Math.floor(elapsedTime / 60);
            elapsedTime %= 60;
        }
        int s = elapsedTime;

        builder.setLength(0);
        builder.append(h > 9 ? "" : "0").append(h);
        builder.append(m > 9 ? ":" : ":0").append(m);
        builder.append(s > 9 ? ":" : ":0").append(s);

        return builder.toString();
    }
}