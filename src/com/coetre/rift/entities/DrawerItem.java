package com.coetre.rift.entities;

public class DrawerItem {
    private final String title;
    private int count;
    private final boolean hasIndicator;

    public DrawerItem(String title, int count, boolean hasIndicator) {
        this.title = title;
        this.count = count;
        this.hasIndicator = hasIndicator;
    }

    public String getTitle() {
        return title;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean hasIndicator() {
        return this.hasIndicator;
    }
}