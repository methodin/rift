package com.coetre.rift.entities;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Manager instance to handle Task objects
 */
public class GlobalEntityChangeManager {
	public static final int ACTION_NEW = 0;
	public static final int ACTION_CHANGE = 1;
	public static final int ACTION_DELETE = 2;
	
	/**
	 * List of Tasks
	 */
	private static final HashMap<String, ArrayList<GlobalEntityChangeListener>> mClasses = new HashMap<String, ArrayList<GlobalEntityChangeListener>>();

	/**
	 * Add a Task to the list
	 * 
	 * @param cls
	 * @param listener
	 */
	public static void add(Class<?> cls, GlobalEntityChangeListener listener) {
		String key = cls.getSimpleName();
		if (!mClasses.containsKey(key)) {
			mClasses.put(key, new ArrayList<GlobalEntityChangeListener>());
		}
		mClasses.get(key).add(listener);
		listener.change(ACTION_NEW);
	}

	/**
	 * Destroy the change listener
	 * 
	 * @param cls
	 * @param listener
	 */
	public static void remove(Class<?> cls, GlobalEntityChangeListener listener) {
		String key = cls.getSimpleName();
		if (mClasses.containsKey(key)) {
			mClasses.get(key).remove(listener);
		}
	}

	/**
	 * Notify all listeners that an item changed
	 * 
	 * @param cls
	 * @param action
	 */
	public static void notifyListeners(Class<?> cls, int action) {
		String key = cls.getSimpleName();
		if (mClasses.containsKey(key)) {
			ArrayList<GlobalEntityChangeListener> listeners = mClasses.get(key);
			for(int i=0;i<listeners.size();i++) {
				if(listeners.get(i) != null) {
					listeners.get(i).change(action);
				}
			}
		}
	}

	public interface GlobalEntityChangeListener {
		/**
		 * Entity class has changed
		 */
		public void change(int action);
	}
}