package com.coetre.rift.entities;

import java.util.Date;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "Log")
public class TaskLog extends Model implements Notifiable {
	/**
	 * The task this log applies to
	 */
	@Column(name = "Task")
	private Task task;

	/**
	 * The start date for the logged activity
	 */
	@Column(name = "StartDate")
	protected Date startDate;

	/**
	 * The end date for the logged activity
	 */
	@Column(name = "EndDate")
	protected Date endDate;
	
	/**
	 * The total number of seconds this log applies to
	 */
	@Column(name = "Seconds")
	protected int seconds;

    /**
     * The invoice this log item was billed on
     */
    @Column(name = "InvoiceRecord")
    protected InvoiceRecord invoiceRecord;

    public TaskLog(Task task, Date startDate, Date endDate, int seconds) {
		super();
		this.task = task;
		this.startDate = startDate;
		this.endDate = endDate;
		this.seconds = seconds;
	}
	public TaskLog() {
		super();
	}

	/**
	 * Getters and Setters
	 */
	public Task getTask() {
		EntityManager<Task> taskManager = EntityManager.getInstance(Task.class);
		return taskManager.get(task);
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public int getSeconds() {
		return seconds;
	}
	
	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}
	
	public void setTask(Task task) {
		this.task = task;
	}

    public InvoiceRecord getInvoiceRecord() {
        return invoiceRecord;
    }

    public void setInvoiceRecord(InvoiceRecord invoiceRecord) {
        this.invoiceRecord = invoiceRecord;
    }

    /**
	 * Get seconds formatted as text
	 * 
	 * @return
	 */
	public String getFormattedTime() {
		double h = 0, m = 0;
		int elapsedTime = getSeconds();
		if (elapsedTime >= 3600) {
			h = Math.floor(elapsedTime / 3600);
			elapsedTime %= 3600;
		}
		if (elapsedTime >= 60) {
			m = Math.floor(elapsedTime / 60);
			elapsedTime %= 60;
		}
		int s = elapsedTime;

		return String.format("%02d:%02d:%02d", (int) h, (int) m, (int) s);
	}

    @Override
    public void invalidate() {

    }

    @Override
    public String getKey() {
        return "TaskLog"+Long.toString(getId());
    }
}