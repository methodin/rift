package com.coetre.rift.entities;

public interface Notifiable {
	public Long getId();
	public void invalidate();
    public String getKey();
}