package com.coetre.rift.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.activeandroid.Model;

/**
 * Manager instance to handle Task objects
 */
public class EntityManager<T> {
	private static HashMap<String, EntityManager<?>> mInstances = new HashMap<String, EntityManager<?>>();

	/**
	 * Object references
	 */
	private HashMap<Long, T> mObjects = new HashMap<Long, T>();

	/**
	 * Returns a new T typed manager
	 * 
	 * @param object
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> EntityManager<T> getInstance(Class<?> object) {
		if (!mInstances.containsKey(object.getSimpleName())) {
			mInstances.put(object.getSimpleName(), new EntityManager<T>());
		}
		return (EntityManager<T>) mInstances.get(object.getSimpleName());
	}

	/**
	 * Add an object to the list
	 * 
	 * @param object
	 * @return
	 */
	public T add(T object) {
		Long key = ((Model) object).getId();
		if (!mObjects.containsKey(key)) {
			mObjects.put(((Model) object).getId(), object);
		}
		return mObjects.get(key);
	}

	/**
	 * Add an object to the list
	 * 
	 * @param object
	 * @return
	 */
	public T get(T object) {
		return mObjects.get(((Model) object).getId());
	}

    public T get(Long id) {
        return mObjects.get(id);
    }

	/**
	 * Filter the list of objects to match the current known list
	 * 
	 * @param list
	 * @return
	 */
	public ArrayList<T> filter(List<T> list) {
		ArrayList<T> newList = new ArrayList<T>();
		for (T object : list) {
			newList.add(add(object));
		}
		return newList;
	}

}