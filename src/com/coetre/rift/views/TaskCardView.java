package com.coetre.rift.views;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.coetre.rift.R;
import com.coetre.rift.ViewTaskActivity;
import com.coetre.rift.actions.TaskActions;
import com.coetre.rift.entities.EntityChangeManager;
import com.coetre.rift.entities.Task;
import com.coetre.rift.utils.Config;
import com.coetre.rift.utils.TaskUpdateManager;
import com.coetre.rift.utils.Tracker;
import com.google.analytics.tracking.android.EasyTracker;

import java.util.List;

public class TaskCardView extends LinearLayout implements OnClickListener, EntityChangeManager.EntityChangeListener {
    private TextView nameView;
    private TextView timeView;
    private TextView childView;
    private Task task;
    private TextView clientLabelView;
    private TextView rateView;
    private ImageButton timerButton;
    private TextView childTimeView;
    private StringBuilder builder = new StringBuilder();
    private List<Task> children;
    private ProgressBar progressIndicator;
    private ProgressBar childProgressIndicator;

    public TaskCardView(Context context, Task task, int resource) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(resource, this, true);

        nameView = (TextView) findViewById(R.id.name);
        timeView = (TextView) findViewById(R.id.time);
        clientLabelView = (TextView) findViewById(R.id.clientLabel);
        rateView = (TextView) findViewById(R.id.rate);
        childView = (TextView) findViewById(R.id.children);
        timerButton = (ImageButton) findViewById(R.id.timerButton);
        childTimeView = (TextView) findViewById(R.id.childTime);
        progressIndicator = (ProgressBar) findViewById(R.id.progressIndicator);
        childProgressIndicator = (ProgressBar) findViewById(R.id.childProgressIndicator);
        this.task = task;

        timeView.setTypeface(Config.thin);
        nameView.setTypeface(Config.thin);
        rateView.setTypeface(Config.thin);
        update();

        timerButton.setOnClickListener(this);
        findViewById(R.id.menuButton).setOnClickListener(this);

        RelativeLayout container = (RelativeLayout) findViewById(R.id.cardContainer);
        container.setClickable(true);
        container.setOnClickListener(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        EntityChangeManager.remove(Task.class, this);
        super.onDetachedFromWindow();
    }

    @Override
    protected void onAttachedToWindow() {
        EntityChangeManager.add(Task.class, this);
        super.onDetachedFromWindow();
    }

    /**
     * Sets the task object
     *
     * @param task
     */
    public void setTask(Task task) {
        this.task = task;
        update();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.timerButton:
                TaskUpdateManager.update(getContext(), task);
                Tracker.trackTask(Tracker.ACTION_TIMER, "TaskCardView");
                break;
            case R.id.menuButton:
                if (task.getParent() == null) {
                    TaskActions.showTaskActions(getContext(), task);
                    Tracker.trackTask(Tracker.ACTION_CLICK, "TaskCardView.task_menu");
                } else {
                    TaskActions.showSubTaskActions(getContext(), task);
                    Tracker.trackTask(Tracker.ACTION_CLICK, "TaskCardView.subtask_menu");
                }
                break;
            default:
                Intent intent = new Intent(getContext(), ViewTaskActivity.class);
                intent.putExtra(ViewTaskActivity.DATA_TASK_ID, task.getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getContext().startActivity(intent);
                break;
        }
    }

    @Override
    public <T> void change(T entity) {
        update((Task) entity);
    }

    /**
     * Updates all the views in this child
     */
    private void update() {
        update(task);
    }

    /**
     * Update all views with the given task
     *
     * @param obj
     */
    private void update(Task obj) {
        if (obj.equals(task)) {
            children = obj.getTasks();

            nameView.setText(obj.getName());
            timeView.setText(obj.getFormattedTime());
            rateView.setText(obj.getFormattedRate());
            childTimeView.setText(obj.getFormattedTime(obj.getChildSeconds()));

            builder.setLength(0);
            builder.append(children.size()).append((children.size() == 1 ? " child" : " children"));
            childView.setText(builder.toString());
            if (obj.getClient() != null) {
                clientLabelView.setBackgroundColor(obj.getClient().getColorAsInt());
                clientLabelView.setVisibility(View.VISIBLE);
            } else if (obj.getParent() != null && obj.getParent().getClient() != null) {
                clientLabelView.setBackgroundColor(obj.getParent().getClient().getColorAsInt());
                clientLabelView.setVisibility(View.VISIBLE);
            } else {
                clientLabelView.setVisibility(View.GONE);
            }


            // Show status indicator
            if (obj.isActive()) {
                progressIndicator.setVisibility(View.VISIBLE);
            } else {
                progressIndicator.setVisibility(View.GONE);
            }

            // Show child status indicator
            if (obj.isChildActive()) {
                childProgressIndicator.setVisibility(View.VISIBLE);
            } else {
                childProgressIndicator.setVisibility(View.GONE);
            }

            // Change timer button
            if (obj.isActive() && timerButton.getTag() == null) {
                timerButton.setImageResource(R.drawable.ic_time_on);
                timerButton.setTag(true);
            } else if (!obj.isActive() && timerButton.getTag() != null) {
                timerButton.setImageResource(R.drawable.ic_time_off);
                timerButton.setTag(null);
            }
        }
    }
}