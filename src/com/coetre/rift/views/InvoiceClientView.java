package com.coetre.rift.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.*;
import com.coetre.rift.R;
import com.coetre.rift.entities.*;
import com.coetre.rift.utils.Config;

public class InvoiceClientView extends LinearLayout {
    public InvoiceClientView(Context context, InvoiceClient invoiceClient) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_invoice_client, this, true);

        TableLayout taskLogs = (TableLayout) findViewById(R.id.taskLogs);
        for(InvoiceRecord invoiceRecord : invoiceClient.getInvoiceRecords()) {
            TableRow row = new TableRow(context);
            row.setLayoutParams(new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            int padding = (int) context.getResources().getDimension(R.dimen.paddingInvoiceRecord);
            row.setPadding(padding, padding, padding, padding);
            // Name
            String extra = "";
            if(invoiceRecord.getTask().getParent() != null) {
                extra = " >> ";
            }
            TextView textView = new TextView(context);
            textView.setTextColor(context.getResources().getColor(R.color.textColor));
            textView.setText(extra + invoiceRecord.getTask().getName());
            textView.setMaxLines(3);
            textView.setHorizontallyScrolling(false);
            textView.setLayoutParams(new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 3f));
            row.addView(textView);

            // Time
            textView = new TextView(context);
            textView.setTextColor(context.getResources().getColor(R.color.textColor));
            textView.setText(invoiceRecord.getFormattedTime());
            //textView.setMaxLines(3);
            //textView.setPadding(padding,padding,padding,padding);
            textView.setLayoutParams(new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f));
            row.addView(textView);

            // Amount
            textView = new TextView(context);
            textView.setTextColor(context.getResources().getColor(R.color.textColor));
            textView.setText(invoiceRecord.getFormattedTotal());
            //textView.setMaxLines(3);
            //textView.setPadding(padding,padding,padding,padding);
            textView.setLayoutParams(new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f));
            row.addView(textView);

            taskLogs.addView(row);
        }
        //taskLogs.setColumnShrinkable(0,true);
        //taskLogs.setStretchAllColumns(false);
        //taskLogs.setShrinkAllColumns(true);
        taskLogs.forceLayout();

        TextView clientView = (TextView) findViewById(R.id.client);
        clientView.setTypeface(Config.thin);
        clientView.setText(invoiceClient.getClient().getName());

        TextView amountView = (TextView) findViewById(R.id.amount);
        amountView.setTypeface(Config.thin);
        amountView.setText(invoiceClient.getFormattedTotal());
    }
}