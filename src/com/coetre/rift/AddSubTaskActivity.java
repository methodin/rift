package com.coetre.rift;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import com.activeandroid.query.Select;
import com.coetre.rift.entities.EntityChangeManager;
import com.coetre.rift.entities.EntityManager;
import com.coetre.rift.entities.GlobalEntityChangeManager;
import com.coetre.rift.entities.Task;
import com.coetre.rift.utils.Alert;
import com.google.analytics.tracking.android.EasyTracker;

public class AddSubTaskActivity extends Activity implements OnClickListener {
	public static final String DATA_TASK_ID = "taskId";
	
	private Task task = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_task);

		findViewById(R.id.cancelButton).setOnClickListener(this);
		findViewById(R.id.addButton).setOnClickListener(this);
		
		if(getIntent().getExtras() != null && getIntent().getExtras().containsKey(DATA_TASK_ID)) {
			long id = getIntent().getExtras().getLong(DATA_TASK_ID, 0);
			task = new Select().from(Task.class).where("Id = ?", id)
					.executeSingle();
			task = (Task) EntityManager.getInstance(Task.class).get(task);
		}
		
		findViewById(R.id.clientListContainer).setVisibility(View.GONE);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.cancelButton:
			setResult(Activity.RESULT_CANCELED);
			finish();
			break;
		case R.id.addButton:
			if(task == null) {
				setResult(Activity.RESULT_CANCELED);
				finish();
			} else {
				String description = ((EditText) findViewById(R.id.taskDescription))
						.getText().toString();
	
				// Form validation
				if (description.equals("")) {
					Alert.show(getApplicationContext(),
							getResources().getString(R.string.taskNameEmpty));
				} else {
					new Task(description, 0, task, null).save();
                    Alert.show(getApplicationContext(), getResources().getString(R.string.taskSaveSuccess));
					GlobalEntityChangeManager.notifyListeners(Task.class, GlobalEntityChangeManager.ACTION_NEW);
                    task.clearList();
					EntityChangeManager.notifyListeners(task);
					setResult(RESULT_OK);
					finish();
				}
			}
			break;
		}
	}

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance().activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance().activityStop(this);
    }
}