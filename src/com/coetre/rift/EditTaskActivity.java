package com.coetre.rift;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import com.activeandroid.query.Select;
import com.coetre.rift.adapters.ClientAdapter;
import com.coetre.rift.entities.*;
import com.coetre.rift.utils.Alert;
import com.google.analytics.tracking.android.EasyTracker;

import java.util.List;

public class EditTaskActivity extends Activity implements OnClickListener {
	public static final String DATA_TASK_ID = "taskId";
	private Task task = null;
	private Spinner list;
	private LinearLayout clientContainer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_task);

		findViewById(R.id.cancelButton).setOnClickListener(this);
		findViewById(R.id.saveButton).setOnClickListener(this);

		if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(DATA_TASK_ID)) {
			long id = getIntent().getExtras().getLong(DATA_TASK_ID, 0);
			task = new Select().from(Task.class).where("Id = ?", id).executeSingle();
			task = (Task) EntityManager.getInstance(Task.class).get(task);
			if (task != null) {
				((EditText) findViewById(R.id.taskDescription)).setText(task.getName());
			}

			clientContainer = (LinearLayout) findViewById(R.id.clientListContainer);
			if (task.getParent() == null) {
				List<Client> clients = new Select().from(Client.class).where("Deleted != 1").execute();
				EntityManager<Client> taskManager = EntityManager.getInstance(Client.class);
				clients = taskManager.filter(clients);
				ClientAdapter adapter = new ClientAdapter(getBaseContext(), clients, true, ClientAdapter.MODE_SELECT);

				list = (Spinner) findViewById(R.id.clientList);
				list.setAdapter(adapter);
				list.setSelection(adapter.getPosition(task.getClient()));
			} else {
				clientContainer.setVisibility(View.GONE);
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.cancelButton:
			setResult(Activity.RESULT_CANCELED);
			finish();
			break;
		case R.id.saveButton:
			if (task == null) {
				setResult(Activity.RESULT_CANCELED);
				finish();
			} else {
				String description = ((EditText) findViewById(R.id.taskDescription)).getText().toString();

				// Form validation
				if (description.equals("")) {
					Alert.show(getApplicationContext(), getResources().getString(R.string.taskNameEmpty));
				} else {
					// Update children views to change client color indicator
					int flags = EntityChangeManager.FLAG_UPDATE_CHILDREN;
					if (clientContainer.getVisibility() != View.GONE) {
						Client client = (Client) list.getSelectedItem();
						client = client != null && client.getId() != null && client.getId() > 0 ? client : null;
						task.setClient(client);
					}
					task.setName(description);
					task.save();

                    Alert.show(getApplicationContext(), getResources().getString(R.string.taskSaveSuccess));
					EntityChangeManager.notifyListeners(task, flags);
					GlobalEntityChangeManager.notifyListeners(Task.class, GlobalEntityChangeManager.ACTION_CHANGE);
					setResult(RESULT_OK);
					finish();
				}
			}
			break;
		}
	}

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance().activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance().activityStop(this);
    }
}