package com.coetre.rift;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.activeandroid.query.Select;
import com.coetre.rift.entities.*;
import com.coetre.rift.utils.Alert;
import com.google.analytics.tracking.android.EasyTracker;

public class EditLogActivity extends Activity implements OnClickListener, AdapterView.OnItemSelectedListener, SeekBar.OnSeekBarChangeListener {
    public static final String DATA_LOG_ID = "taskId";
    private TaskLog log = null;
    private Spinner timeIncrementSpinner;
    private SeekBar timeSelect;
    private EditText amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_log);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(DATA_LOG_ID)) {
            long id = getIntent().getExtras().getLong(DATA_LOG_ID, 0);
            log = new Select().from(TaskLog.class).where("Id = ?", id).executeSingle();
            log = (TaskLog) EntityManager.getInstance(TaskLog.class).get(log);
        }

        findViewById(R.id.cancelButton).setOnClickListener(this);
        findViewById(R.id.saveButton).setOnClickListener(this);
        timeIncrementSpinner = ((Spinner) findViewById(R.id.timeIncrement));
        timeSelect = ((SeekBar)findViewById(R.id.timeSelect));
        amount = ((EditText)findViewById(R.id.amount));

        amount.setText(Integer.toString((int)Math.ceil(log.getSeconds()/60)));
        timeIncrementSpinner.setOnItemSelectedListener(this);
        timeSelect.setOnSeekBarChangeListener(this);

        // Set time increment dropdown options
        String[] timeIncrements = getResources().getStringArray(R.array.timeIncrements);
        ArrayAdapter<String> timeIncrementAdapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, timeIncrements);
        timeIncrementSpinner.setAdapter(timeIncrementAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancelButton:
                setResult(Activity.RESULT_CANCELED);
                finish();
                break;
            case R.id.saveButton:
                boolean finished = true;
                if (log == null) {
                    setResult(Activity.RESULT_CANCELED);
                } else {
                    finished = false;

                    // We log total seconds so multiply by selected increment factor
                    int position = timeIncrementSpinner.getSelectedItemPosition();
                    int multiplier = 60;
                    if(position == 1) {
                        multiplier = 3600;
                    } else if(position == 2) {
                        multiplier = 86400;
                    }

                    String secondsText = amount.getText().toString();
                    try {
                        int adjustedSeconds = Integer.parseInt(secondsText) * multiplier;
                        if(adjustedSeconds < 1) {
                            Alert.show(getBaseContext(), getBaseContext().getResources().getString(R.string.editLogError));
                        } else {
                            Task task = log.getTask();
                            int seconds = log.getSeconds();
                            int diff = adjustedSeconds - seconds;
                            if(diff != 0) {
                                Alert.show(getBaseContext(),(diff>0?"Adding":"Removing")+" "+Math.abs(diff)+" seconds");
                                log.setSeconds(adjustedSeconds);
                                task.incrementSeconds(diff);
                                task.save();
                                log.save();

                                Alert.show(getApplicationContext(), getResources().getString(R.string.logSaveSuccess));
                                EntityChangeManager.notifyListeners(task);
                                EntityChangeManager.notifyListeners(log);
                                GlobalEntityChangeManager.notifyListeners(TaskLog.class, GlobalEntityChangeManager.ACTION_CHANGE);
                                setResult(Activity.RESULT_OK);
                                finished = true;
                            }
                        }
                    } catch(NumberFormatException exception) {
                        Alert.show(getBaseContext(),getBaseContext().getResources().getString(R.string.editLogError));
                    }
                }
                if(finished) {
                    finish();
                }
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        switch(position) {
            case 0:
                timeSelect.setMax(getResources().getInteger(R.integer.timeIncrementMinuteMax));
                break;
            case 1:
                timeSelect.setMax(getResources().getInteger(R.integer.timeIncrementHourMax));
                break;
            case 2:
                timeSelect.setMax(getResources().getInteger(R.integer.timeIncrementDayMax));
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        amount.setText(Integer.toString(i));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance().activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance().activityStop(this);
    }
}