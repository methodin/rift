package com.coetre.rift;

import java.util.ArrayList;

import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.activeandroid.query.Select;
import com.coetre.rift.entities.EntityManager;
import com.coetre.rift.entities.Task;
import com.coetre.rift.actions.TaskActions;
import com.coetre.rift.actions.TaskActions.OnDeleteListener;
import com.google.analytics.tracking.android.EasyTracker;

public class TaskOptionsActivity extends ListActivity {
	public static final String DATA_TASK_ID = "taskId";
	private String[] items;
	private Task task;
	private ArrayList<String> list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if(getIntent().getExtras() != null && getIntent().getExtras().containsKey(DATA_TASK_ID)) {
			long id = getIntent().getExtras().getLong(DATA_TASK_ID, 0);
			task = new Select().from(Task.class).where("Id = ?", id)
					.executeSingle();
			task = (Task) EntityManager.getInstance(Task.class).get(task);
		}
		
		items = getResources().getStringArray(R.array.taskOptions);
		list = new ArrayList<String>();
		for(String string : items) {
			list.add(string);
		}
		
		if(task.isActive()) {
			list.add(getResources().getString(R.string.taskStopTimer));
		} else {
			list.add(getResources().getString(R.string.taskStartTimer));
		}
		
		items = list.toArray(new String[0]);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				getBaseContext(), android.R.layout.simple_list_item_1, items);
		this.setListAdapter(adapter);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		String selected = items[position];
		if(selected.equals(getResources().getString(R.string.taskEdit))) {
			TaskActions.edit(getApplicationContext(), task);
		} else if(selected.equals(getResources().getString(R.string.taskAddSubTask))) {
			TaskActions.addSubTask(getApplicationContext(), task);
			finish();
		} else if(selected.equals(getResources().getString(R.string.taskViewLog))) {
			TaskActions.viewLog(getApplicationContext(), task);
			finish();
		} else if(selected.equals(getResources().getString(R.string.taskDelete))) {
			TaskActions.delete(this, task, new OnDeleteListener(){
				@Override
				public void delete() {
					finish();
				}});
		} else if(selected.equals(getResources().getString(R.string.taskStartTimer)) || selected.equals(getResources().getString(R.string.taskStopTimer))) {
			TaskActions.toggleTimer(getApplicationContext(), task);
			finish();
		} 
		super.onListItemClick(l, v, position, id);
	}

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance().activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance().activityStop(this);
    }
}