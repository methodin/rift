package com.coetre.rift;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.activeandroid.query.Select;
import com.coetre.rift.actions.ClientActions;
import com.coetre.rift.entities.Client;
import com.coetre.rift.entities.EntityManager;
import com.google.analytics.tracking.android.EasyTracker;

import java.util.ArrayList;

public class ClientOptionsActivity extends ListActivity {
    public static final String DATA_CLIENT_ID = "clientId";
    private String[] items;
    private Client client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getIntent().getExtras() != null && getIntent().getExtras().containsKey(DATA_CLIENT_ID)) {
            long id = getIntent().getExtras().getLong(DATA_CLIENT_ID, 0);
            client = new Select().from(Client.class).where("Id = ?", id)
                    .executeSingle();
            client = (Client) EntityManager.getInstance(Client.class).get(client);

            setTitle(getTitle() + " - " + client.getName());
        }

        items = getResources().getStringArray(R.array.clientOptions);
        ArrayList<String> list = new ArrayList<String>();
        for(String string : items) {
            list.add(string);
        }

        items = list.toArray(new String[0]);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getBaseContext(), android.R.layout.simple_list_item_1, items);
        this.setListAdapter(adapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        String selected = items[position];
        if(selected.equals(getResources().getString(R.string.clientDelete))) {
            ClientActions.delete(this, client, new ClientActions.OnDeleteListener() {
                @Override
                public void delete() {
                    finish();
                }
            });
        } else if(selected.equals(getResources().getString(R.string.clientEdit))) {
            ClientActions.edit(this, client);
        }
        super.onListItemClick(l, v, position, id);
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance().activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance().activityStop(this);
    }
}