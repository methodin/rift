package com.coetre.rift;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.*;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import com.activeandroid.query.Select;
import com.coetre.rift.adapters.DrawerAdapter;
import com.coetre.rift.entities.*;
import com.coetre.rift.fragments.*;
import com.coetre.rift.utils.BaseFragmentActivity;
import com.coetre.rift.utils.Config;
import com.coetre.rift.utils.Logger;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseFragmentActivity implements GlobalEntityChangeManager.GlobalEntityChangeListener {
    public final static String DATA_NOTIFICATION = "notification";
    public static Typeface thin = Typeface.create("sans-serif-light", Typeface.NORMAL);

    private SharedPreferences prefs;
    private ListView drawerList;
    private ActionBarDrawerToggle drawerToggle;
    private DrawerLayout drawerLayout;
    private String[] drawerItems;
    private CharSequence title = "";
    private Fragment fragment = null;
    private boolean isDrawerLocked = false;

    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_main);

        prefs = getBaseContext().getSharedPreferences(Config.PREFS_NAME, Context.MODE_PRIVATE);

        if(getIntent().getExtras() != null && getIntent().getExtras().containsKey(DATA_NOTIFICATION)) {
            getIntent().removeExtra(DATA_NOTIFICATION);
            SharedPreferences.Editor editor = prefs.edit();
            Config.mode = Config.MODE_TASK;
            editor.putInt(Config.PREFS_KEY_MODE, Config.mode);
            editor.commit();
        }

        Config.sort = prefs.getInt(Config.PREFS_KEY_SORT, Config.SORT_DATE_ASC);
        Config.mode = prefs.getInt(Config.PREFS_KEY_MODE, Config.MODE_TASK);

        drawerList = (ListView) findViewById(R.id.left_drawer);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        FrameLayout frameLayout = (FrameLayout)findViewById(R.id.content_frame);
        if(((ViewGroup.MarginLayoutParams)frameLayout.getLayoutParams()).leftMargin == (int)getResources().getDimension(R.dimen.drawerSize)) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN, drawerList);
            drawerLayout.setScrimColor(Color.TRANSPARENT);
            isDrawerLocked = true;
        }

        // Set the adapter for the list view
        drawerItems = getResources().getStringArray(R.array.drawerOptions);

        drawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.actionSort,  /* "open drawer" description */
               R.string.actionSort  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(title);
                ((FragmentInterface)fragment).showMenuActions();
                invalidateOptionsMenu();
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle("Select Option");
                ((FragmentInterface)fragment).hideMenuActions();
                invalidateOptionsMenu();
            }
        };

        if(!isDrawerLocked) {
            drawerLayout.setDrawerListener(drawerToggle);
        }

        ArrayList<DrawerItem> drawerListValues = new ArrayList<DrawerItem>();
        for(String drawerItem : drawerItems) {
            if(drawerItem.equals(getResources().getString(R.string.drawerSettings))) {
                drawerListValues.add(new DrawerItem(drawerItem, 0, false));
            } else {
                drawerListValues.add(new DrawerItem(drawerItem, 0, true));
            }
        }
        drawerList.setAdapter(new DrawerAdapter(this,
                R.layout.drawer_list_item, drawerListValues));

        // Set the drawer toggle as the DrawerListener
        DrawerItemClickListener drawerItemClickListener = new DrawerItemClickListener();
        drawerList.setOnItemClickListener(drawerItemClickListener);
        drawerItemClickListener.onItemClick(null, null, Config.mode, 0);

        if(!isDrawerLocked) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }

        Intent intent = new Intent();
        intent.setAction("com.coetre.rift.intent.action.START_TIMER");
        sendBroadcast(intent);
    }

    @Override
    public void setTitle(CharSequence title) {
        this.title = title;
        getActionBar().setTitle(title);
    }

    @Override
    protected void sortClicked(int sort) {
		Config.sort = sort;
		SharedPreferences.Editor editor = prefs.edit();
		editor.putInt(Config.PREFS_KEY_SORT, Config.sort);
		editor.commit();

        ((FragmentInterface)fragment).dispatchSort(Config.sort);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = drawerLayout.isDrawerOpen(drawerList);
        MenuItem item = menu.findItem(R.id.actionAdd);
        if(item != null) {
            item.setVisible(!drawerOpen);
        }
        item = menu.findItem(R.id.actionSort);
        if(item != null) {
            item.setVisible(!drawerOpen);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();
        GlobalEntityChangeManager.add(Task.class, this);
        updateDrawerItems();
    }

    @Override
    protected void onPause() {
        super.onPause();
        GlobalEntityChangeManager.remove(Task.class, this);
    }

    /**
     * Update the drawer items
     */
    private void updateDrawerItems() {
        new DrawerLoader(drawerList).execute();
    }

    @Override
    public void change(int action) {
        updateDrawerItems();
    }

    public class DrawerLoader extends AsyncTask<Integer, Void, Integer[]> {
        private final WeakReference<ListView> referenceDrawerList;

        public DrawerLoader(ListView drawerList) {
            referenceDrawerList = new WeakReference<ListView>(drawerList);
        }

        @Override
        protected Integer[] doInBackground(Integer... params) {
            Integer[] counts = new Integer[4];
            List<Task> tasks = new Select().from(Task.class).where("Deleted != 1").execute();
            counts[0] = tasks.size();
            tasks = new Select().from(Task.class).where("Deleted != 1 AND Seconds > 0").execute();
            counts[1] = tasks.size();
            List<Client> clients = new Select().from(Client.class).where("Deleted != 1").execute();
            counts[2] = clients.size();
            List<Invoice> invoices = new Select().from(Invoice.class).execute();
            counts[3] = invoices.size();
            return counts;
        }

        /**
         * Set the ListView items in the UI thread
         */
        @Override
        protected void onPostExecute(Integer[] counts) {
            final ListView drawerList = referenceDrawerList.get();
            if(drawerList != null) {
                DrawerAdapter adapter = (DrawerAdapter) drawerList.getAdapter();
                for(int i=0;i<counts.length;i++) {
                    adapter.getItem(i).setCount(counts[i]);
                }
                adapter.notifyDataSetChanged();
            }
        }
    }

    /**
     * The drawer item click listener
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }

        /** Swaps fragments in the main content view */
        private void selectItem(int position) {
            Bundle bundle;
            switch(position) {
                case Config.MODE_TASK:
                    fragment = new TaskCardFragment();
                    bundle = new Bundle();
                    bundle.putInt(TaskCardFragment.DATA_TYPE, Config.MODE_TASK);
                    fragment.setArguments(bundle);
                    break;
                case Config.MODE_ACTIVE:
                    fragment = new TaskCardFragment();
                    bundle = new Bundle();
                    bundle.putInt(TaskCardFragment.DATA_TYPE, Config.MODE_ACTIVE);
                    fragment.setArguments(bundle);
                    break;
                case Config.MODE_CLIENTS:
                    fragment = new ClientListFragment();
                    break;
                case Config.MODE_INVOICE:
                    fragment = new InvoiceListFragment();
                    break;
                case Config.MODE_SETTINGS:
                    drawerList.getOnItemClickListener().onItemClick(null, null, Config.mode, 0);
                    Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                    startActivity(intent);
                    return;
            }
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .commit();

            // Highlight the selected item, update the title, and close the drawer
            drawerList.setItemChecked(position, true);
            setTitle(MainActivity.this.drawerItems[position]);
            if(!isDrawerLocked) {
                drawerLayout.closeDrawer(drawerList);
            }

            Config.mode = position;
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt(Config.PREFS_KEY_MODE, position);
            editor.commit();
        }
    }
}
