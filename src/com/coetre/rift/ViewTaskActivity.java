package com.coetre.rift;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.activeandroid.query.Select;
import com.coetre.rift.actions.ClientActions;
import com.coetre.rift.actions.TaskActions;
import com.coetre.rift.adapters.TaskCardAdapter;
import com.coetre.rift.entities.*;
import com.coetre.rift.loaders.TaskLoader;
import com.coetre.rift.utils.Config;
import com.coetre.rift.utils.Logger;
import com.coetre.rift.utils.TaskUpdateManager;
import com.google.analytics.tracking.android.EasyTracker;

import java.util.List;

public class ViewTaskActivity extends Activity implements EntityChangeManager.EntityChangeListener {
    public final static String DATA_TASK_ID = "taskId";
    private TextView timeView;
    private TextView nameView;
    private TextView rateView;
    private Task task;
    private TextView clientLabelView;
    private ImageView timerView;
    private GridView gridView;
    private View emptyView;
    private ProgressBar progressIndicator;
    private List<Task> lastChildTaskList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_task);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        timeView = (TextView) findViewById(R.id.time);
        nameView = (TextView) findViewById(R.id.name);
        rateView = (TextView) findViewById(R.id.rate);
        progressIndicator = (ProgressBar) findViewById(R.id.progressIndicator);
        clientLabelView = (TextView) findViewById(R.id.clientLabel);
        timeView.setTypeface(Config.thin);
        rateView.setTypeface(Config.thin);
        gridView = (GridView) findViewById(R.id.cardList);
        TaskCardAdapter adapter = new TaskCardAdapter(getBaseContext(), R.layout.view_task_card, null);
        gridView.setAdapter(adapter);
        emptyView = findViewById(android.R.id.empty);

        if(getIntent().getExtras() != null&& getIntent().getExtras().containsKey(DATA_TASK_ID)) {
            new TaskLoader(new TaskLoader.OnTaskLoaderCompleted() {
                @Override
                public void onTaskLoaderCompleted(Task task) {
                    ViewTaskActivity.this.task = task;
                    invalidateOptionsMenu();
                    update();
                }
            }).execute(new Select().from(Task.class).where("Id = ?", getIntent().getExtras().getLong(DATA_TASK_ID)));
        }

        EntityChangeManager.add(Task.class, this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.actionEdit:
                TaskActions.edit(getApplicationContext(), task);
                break;
            case R.id.actionAddSubTask:
                TaskActions.addSubTask(getApplicationContext(), task);
                break;
            case R.id.actionViewLog:
                TaskActions.viewLog(getApplicationContext(), task);
                break;
            case R.id.actionDelete:
                TaskActions.delete(this, task, new TaskActions.OnDeleteListener() {
                    @Override
                    public void delete() {
                        finish();
                    }
                });
                break;
            case R.id.actionTimer:
                TaskUpdateManager.update(getBaseContext(), task);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        if(task != null && task.getParent() == null) {
            getMenuInflater().inflate(R.menu.view_task, menu);
        } else {
            getMenuInflater().inflate(R.menu.view_subtask, menu);
        }
        timerView= new ImageView(getBaseContext(),null);
        timerView.setImageResource(R.drawable.ic_time_off);
        timerView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewTaskActivity.this.onOptionsItemSelected(menu.findItem(R.id.actionTimer));
            }
        });
        menu.findItem(R.id.actionTimer).setActionView(timerView);
        return true;
    }

    @Override
    public <T> void change(T entity) {
        update((Task)entity);
    }

    /**
     * Update all views with stock task
     */
    public void update() {
        update(task);
    }

    /**
     * Update all views with the given task
     * @param obj
     */
    public void update(Task obj) {
        if(task != null && task.equals(obj)) {
            timeView.setText(obj.getFormattedTime());
            nameView.setText(obj.getName());
            rateView.setText(obj.getFormattedRate());

            // Reload children
            if(lastChildTaskList == null || (task.getTasks() != null && !task.getTasks().equals(lastChildTaskList))) {
                ((TaskCardAdapter)gridView.getAdapter()).setTasks(task.getTasks());
                lastChildTaskList = task.getTasks();
                if(lastChildTaskList.size() > 0) {
                    gridView.setVisibility(View.VISIBLE);
                    emptyView.setVisibility(View.GONE);
                } else {
                    gridView.setVisibility(View.GONE);
                    emptyView.setVisibility(View.VISIBLE);
                }
            }

            if(obj.isActive()) {
                progressIndicator.setVisibility(View.VISIBLE);
            } else {
                progressIndicator.setVisibility(View.GONE);
            }

            if(timerView != null) {
                // Change timer button
                if (obj.isActive() && timerView.getTag() == null) {
                    timerView.setImageResource(R.drawable.ic_time_on);
                    timerView.setTag(true);
                } else if (!obj.isActive() && timerView.getTag() != null) {
                    timerView.setImageResource(R.drawable.ic_time_off);
                    timerView.setTag(null);
                }
            }

            if(obj.getClient() != null) {
                String extra = "";
                if(obj.getClient().getRate() > 0) {
                    extra = " - Rate: $"+Integer.toString(obj.getClient().getRate())+"/hr";
                }
                clientLabelView.setText(obj.getClient().getName()+extra);
                clientLabelView.setBackgroundColor(obj.getClient().getColorAsInt());
                clientLabelView.setVisibility(View.VISIBLE);
            } else {
                clientLabelView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance().activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance().activityStop(this);
    }
}