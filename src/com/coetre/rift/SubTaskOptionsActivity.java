package com.coetre.rift;

import java.util.ArrayList;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.activeandroid.query.Select;
import com.coetre.rift.entities.EntityManager;
import com.coetre.rift.entities.Task;
import com.coetre.rift.actions.TaskActions;
import com.google.analytics.tracking.android.EasyTracker;

public class SubTaskOptionsActivity extends ListActivity {
	public static final String DATA_TASK_ID = "taskId";
	private String[] mItems;
	private Task mTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if(getIntent().getExtras() != null && getIntent().getExtras().containsKey(DATA_TASK_ID)) {
			long id = getIntent().getExtras().getLong(DATA_TASK_ID, 0);
			mTask = new Select().from(Task.class).where("Id = ?", id)
					.executeSingle();
			mTask = (Task) EntityManager.getInstance(Task.class).get(mTask);
		}
		
		mItems = getResources().getStringArray(R.array.subTaskOptions);
        ArrayList<String> list = new ArrayList<String>();
		for(String string : mItems) {
            list.add(string);
		}
		
		if(mTask.isActive()) {
            list.add(getResources().getString(R.string.subTaskStopTimer));
		} else {
            list.add(getResources().getString(R.string.subTaskStartTimer));
		}
		
		mItems = list.toArray(new String[0]);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				getBaseContext(), android.R.layout.simple_list_item_1, mItems);
		this.setListAdapter(adapter);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		String selected = mItems[position];
		if(selected.equals(getResources().getString(R.string.subTaskEdit))) {
			TaskActions.edit(getApplicationContext(), mTask);
		} else if(selected.equals(getResources().getString(R.string.subTaskStartTimer)) || selected.equals(getResources().getString(R.string.subTaskStopTimer))) {
			TaskActions.toggleTimer(getApplicationContext(), mTask);
			finish();
		} else if(selected.equals(getResources().getString(R.string.subTaskViewLog))) {
			TaskActions.viewLog(getApplicationContext(), mTask);
		} else if(selected.equals(getResources().getString(R.string.subTaskDelete))) {
            TaskActions.delete(SubTaskOptionsActivity.this, mTask, new TaskActions.OnDeleteListener(){
                @Override
                public void delete() {
                    finish();
                }
            });
        }
		
		super.onListItemClick(l, v, position, id);
	}

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance().activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance().activityStop(this);
    }
}